package MODULI::GUI_ENTRATA_MERCI_INT_SAP;

use strict;
use warnings;
use utf8;
use Carp qw(verbose);
use CGI;
use Data::Dumper;
use DateTime;
use JSON;
use JSON::PP;

use lib "$ENV{HOME}"."/ART/Appl";

use ART::C_db_prod_art;

my $TRACE = sub { do { local $| = 1;  print STDERR join( "", @_, "\n" ) } if 1 };

sub new { 
    my ( $this, $param ) = @_;
    my $class = ref($this) || $this;
    my $self = {};
 
    $self->{db}             = exists $param->{db}            ? $param->{db}             : ART::C_db_prod_art->new({DEBUG => 1});
    $self->{id_operatore}   = exists $param->{id_operatore}  ? $param->{id_operatore}   : 0;
    $self->{id_sessione}    = exists $param->{id_sessione}   ? $param->{id_sessione}    : '';
    $self->{debug}          = exists $param->{debug}         ? $param->{debug}          : 0;
   
    $self = bless $self, $class;
   

   
    return $self;
}

sub _db                           { shift->{db} }
sub _id_operatore                 { shift->{id_operatore} }
sub _id_session                   { shift->{id_sessione}  }
sub _get_tipo_operatore           { shift->{tipo_operatore} }



#S&R WIND: procedura che ritorna la lista delle attivita che popola la griglia dell UI

sub getGridSwapRepairWind{
  my( $self, $param ) = @_;
  my $res;
	
	my $querySql="";
	
	print STDERR Dumper $param;
	
	my $sOperatore = $param->{'S_OPERATORE'};
	
	$querySql = sprintf q{SELECT 
	v.RIPARATORE,
	v.ID_CATALOGO,
	v.TIPO,
	v.ID_ATTIVITA,
	v.STATO,
	TO_CHAR(v.DATA_ULTIMA_AZIONE,'DD/MM/YYYY HH24:MI:SS') DATA_ULTIMA_AZIONE,
	v.ID_RMA_RIPARATORE,
	v.CODICE_SIRTI_RICHIESTO,
	v.PART_NUMBER_RICHIESTO,
	v.SAP_CODE,
	v.QUANTITA,
	v.MAGAZZINO,
	v.RIF_WIND,
	v.CELLULARE_RIF_WIND	
	FROM V_EM_SWAP_REPAIR_WIND v};		


	# condizioni custom by operatore
		
	if ($sOperatore ne 'ROOT'){
		# faccio logica per MAGAZZINIERE e PM che vedono solo il loro MAGAZZINI
		my $checkOperatore =  $self->_db->fetch_minimalized("SELECT count(*) esito 
			FROM GRUPPI g 
			where G.ID_GRUPPO in (
				SELECT OG.ID_GRUPPO 
				FROM OPERATORI_GRUPPI og 
				WHERE og.ID_OPERATORE IN (
					SELECT o.ID_OPERATORE 
					FROM OPERATORI o
					where upper(o.LOGIN_OPERATORE) = upper('$sOperatore')
					)
			) and upper(G.NOME) in ('MAGAZZINIERE')");
  
		if ($checkOperatore > 0){
			$querySql .= q{ where upper(V.MAGAZZINO) in (
														/* magazzini dell'operatore */
														SELECT SUBSTR(G.NOME,2) 
														FROM GRUPPI g 
														where G.ID_GRUPPO in (
															SELECT OG.ID_GRUPPO 
															FROM OPERATORI_GRUPPI og 
															WHERE og.ID_OPERATORE IN (
																SELECT o.ID_OPERATORE 
																FROM OPERATORI o
																where upper(o.LOGIN_OPERATORE) = upper('%s')
																)
														) and (upper(G.NOME) like 'M1%' or  upper(G.NOME) like 'M2%'))},$sOperatore;
		}
	}
	
	eval {
		my  $stmt = $self->_db->create_prepare(
			$querySql
		);
	
		$res->{data} = $stmt->fetchall_hashref();
	};
	if( $@ ) {
		$res->{ErrDesc} = $@;
		$res->{ErrCode} = -1;
	}
	
		return $res;

}



# ADC: servizio che ritorna la lista delle attivita che popola la griglia dell UI

sub getGridAdcPolarys{
  my( $self, $param ) = @_;
  my $res;
	
	my $querySql="";
	
	print STDERR Dumper $param;
	
	my $sOperatore = $param->{'S_OPERATORE'};
	
	$querySql = sprintf q{select
	v.TIPO,
	v.ID_ATTIVITA,
	v.STATO,
	TO_CHAR(v.DATA_ULTIMA_AZIONE,'DD/MM/YYYY HH24:MI:SS') DATA_ULTIMA_AZIONE,
	TO_CHAR(v.DATA_PREVISTA_CONSEGNA,'DD/MM/YYYY HH24:MI:SS') DATA_PREVISTA_CONSEGNA,
	v.CODICE_SIRTI,
	v.PART_NUMBER,
	v.SAP_CODE,
	v.QUANTITA,
	v.OPERAZIONE,
	v.MAGAZZINO,
	v.FORNITORE,
	v.PROGETTO,
	v.SOTTO_PROGETTO,
	v.PROPRIETA,
	v.CLIENTE_FINALE,
	v.ID_CATALOGO
	FROM V_EM_ADC_POLARYS v};		


	# condizioni custom by operatore
		
	if ($sOperatore ne 'ROOT'){
		# faccio logica per MAGAZZINIERE e PM che vedono solo il loro MAGAZZINI
		my $checkOperatore =  $self->_db->fetch_minimalized("SELECT count(*) esito 
			FROM GRUPPI g 
			where G.ID_GRUPPO in (
				SELECT OG.ID_GRUPPO 
				FROM OPERATORI_GRUPPI og 
				WHERE og.ID_OPERATORE IN (
					SELECT o.ID_OPERATORE 
					FROM OPERATORI o
					where upper(o.LOGIN_OPERATORE) = upper('$sOperatore')
					)
			) and upper(G.NOME) in ('MAGAZZINIERE')");
  
		if ($checkOperatore > 0){
			$querySql .= q{ where upper(V.MAGAZZINO) in (
														/* magazzini dell'operatore */
														SELECT SUBSTR(G.NOME,2) 
														FROM GRUPPI g 
														where G.ID_GRUPPO in (
															SELECT OG.ID_GRUPPO 
															FROM OPERATORI_GRUPPI og 
															WHERE og.ID_OPERATORE IN (
																SELECT o.ID_OPERATORE 
																FROM OPERATORI o
																where upper(o.LOGIN_OPERATORE) = upper('%s')
																)
														) and (upper(G.NOME) like 'M1%' or  upper(G.NOME) like 'M2%'))},$sOperatore;
		}
	}
	
	eval {
		my  $stmt = $self->_db->create_prepare(
			$querySql
		);
	
		$res->{data} = $stmt->fetchall_hashref();
	};
	if( $@ ) {
		$res->{ErrDesc} = $@;
		$res->{ErrCode} = -1;
	}
	
		return $res;

}



# RESO DA CAMPO: servizio che ritorna la lista delle attivita che popola la griglia dell UI

sub getGridResoPolarys{
  my( $self, $param ) = @_;
  my $res;
	
	my $querySql="";
	
	print STDERR Dumper $param;
	
	my $sOperatore = $param->{'S_OPERATORE'};
	
	$querySql = sprintf q{select
	v.TIPO,
	v.ID_ATTIVITA,
	v.STATO,
	TO_CHAR(v.DATA_ULTIMA_AZIONE,'DD/MM/YYYY HH24:MI:SS') DATA_ULTIMA_AZIONE,
	v.CODICE_SIRTI,
	v.ID_SIRTI,
	v.PART_NUMBER,
	v.SERIAL_NUMBER,
	v.QUANTITA,
	v.SQUADRA_MAT,
	v.STATO_MAT,
	v.MAGAZZINO,
	v.PROGETTO,
	v.SOTTO_PROGETTO,
	v.PROPRIETA,
	v.CLIENTE_FINALE,
	v.ID_CATALOGO
	FROM V_EM_RESO_POLARYS v};		


	# condizioni custom by operatore
		
	if ($sOperatore ne 'ROOT'){
		# faccio logica per MAGAZZINIERE e PM che vedono solo il loro MAGAZZINI
		my $checkOperatore =  $self->_db->fetch_minimalized("SELECT count(*) esito 
			FROM GRUPPI g 
			where G.ID_GRUPPO in (
				SELECT OG.ID_GRUPPO 
				FROM OPERATORI_GRUPPI og 
				WHERE og.ID_OPERATORE IN (
					SELECT o.ID_OPERATORE 
					FROM OPERATORI o
					where upper(o.LOGIN_OPERATORE) = upper('$sOperatore')
					)
			) and upper(G.NOME) in ('MAGAZZINIERE')");
  
		if ($checkOperatore > 0){
			$querySql .= q{ where upper(V.MAGAZZINO) in (
														/* magazzini dell'operatore */
														SELECT SUBSTR(G.NOME,2) 
														FROM GRUPPI g 
														where G.ID_GRUPPO in (
															SELECT OG.ID_GRUPPO 
															FROM OPERATORI_GRUPPI og 
															WHERE og.ID_OPERATORE IN (
																SELECT o.ID_OPERATORE 
																FROM OPERATORI o
																where upper(o.LOGIN_OPERATORE) = upper('%s')
																)
														) and (upper(G.NOME) like 'M1%' or  upper(G.NOME) like 'M2%'))},$sOperatore;
		}
	}
	
	eval {
		my  $stmt = $self->_db->create_prepare(
			$querySql
		);
	
		$res->{data} = $stmt->fetchall_hashref();
	};
	if( $@ ) {
		$res->{ErrDesc} = $@;
		$res->{ErrCode} = -1;
	}
	
		return $res;

}


# RIENTRO RIPARAZIONE: servizio che ritorna la lista delle attivita che popola la griglia dell UI

sub getGridRientroRiparazione{
  my( $self, $param ) = @_;
  my $res;
	
	my $querySql="";
	
	print STDERR Dumper $param;
	
	my $sOperatore = $param->{'S_OPERATORE'};
	
	$querySql = sprintf q{select
	v.CLIENTE_FINALE,
	v.ID_CATALOGO,
	v.TIPO,
	v.ID_ATTIVITA,
	v.STATO,
	v.DATA_APERTURA,
	v.ID_RMA_RIPARATORE,
	TO_CHAR(v.DATA_SPEDIZIONE_RIPARATORE,'DD/MM/YYYY HH24:MI:SS') DATA_SPEDIZIONE_RIPARATORE,
	v.MAGAZZINO,
	v.ID_SIRTI,
	v.CODICE_SIRTI,
	v.PART_NUMBER,
	v.SAP_CODE,
	v.SERIAL_NUMBER,
	v.RIPARATORE,
	v.INDIRIZZO_RIPARATORE,
	v.DDT,
	TO_CHAR(v.DATA_DDT,'DD/MM/YYYY HH24:MI:SS') DATA_DDT,
	v.PROGETTO,
	v.SOTTO_PROGETTO,
	v.PROPRIETA
	FROM V_EM_RIENTRO_RIPARAZIONE v};		


	# condizioni custom by operatore
		
	if ($sOperatore ne 'ROOT'){
		# faccio logica per MAGAZZINIERE e PM che vedono solo il loro MAGAZZINI
		my $checkOperatore =  $self->_db->fetch_minimalized("SELECT count(*) esito 
			FROM GRUPPI g 
			where G.ID_GRUPPO in (
				SELECT OG.ID_GRUPPO 
				FROM OPERATORI_GRUPPI og 
				WHERE og.ID_OPERATORE IN (
					SELECT o.ID_OPERATORE 
					FROM OPERATORI o
					where upper(o.LOGIN_OPERATORE) = upper('$sOperatore')
					)
			) and upper(G.NOME) in ('MAGAZZINIERE')");
  
		if ($checkOperatore > 0){
			$querySql .= q{ where upper(V.MAGAZZINO) in (
														/* magazzini dell'operatore */
														SELECT SUBSTR(G.NOME,2) 
														FROM GRUPPI g 
														where G.ID_GRUPPO in (
															SELECT OG.ID_GRUPPO 
															FROM OPERATORI_GRUPPI og 
															WHERE og.ID_OPERATORE IN (
																SELECT o.ID_OPERATORE 
																FROM OPERATORI o
																where upper(o.LOGIN_OPERATORE) = upper('%s')
																)
														) and (upper(G.NOME) like 'M1%' or  upper(G.NOME) like 'M2%'))},$sOperatore;
		}
	}
	
	eval {
		my  $stmt = $self->_db->create_prepare(
			$querySql
		);
	
		$res->{data} = $stmt->fetchall_hashref();
	};
	if( $@ ) {
		$res->{ErrDesc} = $@;
		$res->{ErrCode} = -1;
	}
	
		return $res;

}

# RIENTRO RIPARAZIONE: servizio che ritorna la lista delle attivita che popola la griglia dell UI

sub getGridRientroRiparazione{
  my( $self, $param ) = @_;
  my $res;
	
	my $querySql="";
	
	print STDERR Dumper $param;
	
	my $sOperatore = $param->{'S_OPERATORE'};
	
	$querySql = sprintf q{select
	v.CLIENTE_FINALE,
	v.ID_CATALOGO,
	v.TIPO,
	v.ID_ATTIVITA,
	v.STATO,
	TO_CHAR(v.DATA_APERTURA,'DD/MM/YYYY HH24:MI:SS') DATA_APERTURA,
	v.ID_RMA_RIPARATORE,
	TO_CHAR(v.DATA_SPEDIZIONE_RIPARATORE,'DD/MM/YYYY HH24:MI:SS') DATA_SPEDIZIONE_RIPARATORE,
	v.MAGAZZINO,
	v.ID_SIRTI,
	v.CODICE_SIRTI,
	v.PART_NUMBER,
	v.SAP_CODE,
	v.SERIAL_NUMBER,
	v.RIPARATORE,
	v.INDIRIZZO_RIPARATORE,
	v.DDT,
	TO_CHAR(v.DATA_DDT,'DD/MM/YYYY HH24:MI:SS') DATA_DDT,
	v.PROGETTO,
	v.SOTTO_PROGETTO,
	v.PROPRIETA
	FROM V_EM_RIENTRO_RIPARAZIONE v};		


	# condizioni custom by operatore
		
	if ($sOperatore ne 'ROOT'){
		# faccio logica per MAGAZZINIERE e PM che vedono solo il loro MAGAZZINI
		my $checkOperatore =  $self->_db->fetch_minimalized("SELECT count(*) esito 
			FROM GRUPPI g 
			where G.ID_GRUPPO in (
				SELECT OG.ID_GRUPPO 
				FROM OPERATORI_GRUPPI og 
				WHERE og.ID_OPERATORE IN (
					SELECT o.ID_OPERATORE 
					FROM OPERATORI o
					where upper(o.LOGIN_OPERATORE) = upper('$sOperatore')
					)
			) and upper(G.NOME) in ('MAGAZZINIERE')");
  
		if ($checkOperatore > 0){
			$querySql .= q{ where upper(V.MAGAZZINO) in (
														/* magazzini dell'operatore */
														SELECT SUBSTR(G.NOME,2) 
														FROM GRUPPI g 
														where G.ID_GRUPPO in (
															SELECT OG.ID_GRUPPO 
															FROM OPERATORI_GRUPPI og 
															WHERE og.ID_OPERATORE IN (
																SELECT o.ID_OPERATORE 
																FROM OPERATORI o
																where upper(o.LOGIN_OPERATORE) = upper('%s')
																)
														) and (upper(G.NOME) like 'M1%' or  upper(G.NOME) like 'M2%'))},$sOperatore;
		}
	}
	
	eval {
		my  $stmt = $self->_db->create_prepare(
			$querySql
		);
	
		$res->{data} = $stmt->fetchall_hashref();
	};
	if( $@ ) {
		$res->{ErrDesc} = $@;
		$res->{ErrCode} = -1;
	}
	
		return $res;

}

# RITIRI WIND - tab RESI : servizio che ritorna la lista delle attivita che popola la griglia dell UI

sub getGridResoWind{
  my( $self, $param ) = @_;
  my $res;
	
	my $querySql="";
	
	print STDERR Dumper $param;
	
	my $sOperatore = $param->{'S_OPERATORE'};
	
	$querySql = sprintf q{select
	v.TIPO,
	v.TIPO_GESTIONE,
	v.ID_ATTIVITA,
	v.STATO,
	TO_CHAR(v.DATA_APERTURA,'DD/MM/YYYY HH24:MI:SS') DATA_APERTURA,
	TO_CHAR(v.DATA_APPUNTAMENTO_RITIRO_RESO,'DD/MM/YYYY HH24:MI:SS') DATA_APPUNTAMENTO_RITIRO_RESO,
	v.MAGAZZINO_WIND_PARTENZA,
	v.MAGAZZINO_SIRTI,
	v.ID_SIRTI,
	v.CODICE_SIRTI,
	v.PART_NUMBER,
	v.SAP_CODE,
	v.CAUSALE_RESO,
	v.VINCOLO_CONSEGNA_CONCORDATA,
	v.RIF_WIND,
	v.CELLULARE_RIF_WIND,
	v.ID_CATALOGO,
	v.OPERAZIONE_SU_MATERIALE,
	v.TIPO_MATERIALE
	FROM V_EM_RESO_WIND v};		


	# condizioni custom by operatore
		
	if ($sOperatore ne 'ROOT'){
		# faccio logica per MAGAZZINIERE e PM che vedono solo il loro MAGAZZINI
		my $checkOperatore =  $self->_db->fetch_minimalized("SELECT count(*) esito 
			FROM GRUPPI g 
			where G.ID_GRUPPO in (
				SELECT OG.ID_GRUPPO 
				FROM OPERATORI_GRUPPI og 
				WHERE og.ID_OPERATORE IN (
					SELECT o.ID_OPERATORE 
					FROM OPERATORI o
					where upper(o.LOGIN_OPERATORE) = upper('$sOperatore')
					)
			) and upper(G.NOME) in ('MAGAZZINIERE')");
  
		if ($checkOperatore > 0){
			$querySql .= q{ where upper(V.MAGAZZINO) in (
														/* magazzini dell'operatore */
														SELECT SUBSTR(G.NOME,2) 
														FROM GRUPPI g 
														where G.ID_GRUPPO in (
															SELECT OG.ID_GRUPPO 
															FROM OPERATORI_GRUPPI og 
															WHERE og.ID_OPERATORE IN (
																SELECT o.ID_OPERATORE 
																FROM OPERATORI o
																where upper(o.LOGIN_OPERATORE) = upper('%s')
																)
														) and (upper(G.NOME) like 'M1%' or  upper(G.NOME) like 'M2%'))},$sOperatore;
		}
	}
	
	eval {
		my  $stmt = $self->_db->create_prepare(
			$querySql
		);
	
		$res->{data} = $stmt->fetchall_hashref();
	};
	if( $@ ) {
		$res->{ErrDesc} = $@;
		$res->{ErrCode} = -1;
	}
	
		return $res;

}

# RITIRI WIND - tab TRASFERIMENTI : servizio che ritorna la lista delle attivita che popola la griglia dell UI

sub getGridTrasferimentoWind{
  my( $self, $param ) = @_;
  my $res;
	
	my $querySql="";
	
	print STDERR Dumper $param;
	
	my $sOperatore = $param->{'S_OPERATORE'};
	
	$querySql = sprintf q{select
	v.TIPO,
	v.TIPO_GESTIONE,
	v.ID_ATTIVITA,
	v.STATO,
	TO_CHAR(v.DATA_APERTURA,'DD/MM/YYYY HH24:MI:SS') DATA_APERTURA,
	TO_CHAR(v.DATA_PREVISTA_CONSEGNA,'DD/MM/YYYY HH24:MI:SS') DATA_PREVISTA_CONSEGNA,	
	v.QUANTITA,
	v.ID_SIRTI,
	v.CODICE_SIRTI,
	v.PART_NUMBER,
	v.SAP_CODE,
	v.SERIAL_NUMBER,
	v.SLA,
	v.MAGAZZINO_PARTENZA,
	v.MAGAZZINO_SIRTI,
	v.DDT,
	v.DATA_DDT,
	v.RIF_WIND,
	v.CELLULARE_RIF_WIND,
	v.PROGETTO,
	v.SOTTO_PROGETTO,
	v.PROPRIETA,
	v.CLIENTE_FINALE,
	v.ID_CATALOGO
	FROM V_EM_TRASFERIMENTO_WIND v};		


	# condizioni custom by operatore
		
	if ($sOperatore ne 'ROOT'){
		# faccio logica per MAGAZZINIERE e PM che vedono solo il loro MAGAZZINI
		my $checkOperatore =  $self->_db->fetch_minimalized("SELECT count(*) esito 
			FROM GRUPPI g 
			where G.ID_GRUPPO in (
				SELECT OG.ID_GRUPPO 
				FROM OPERATORI_GRUPPI og 
				WHERE og.ID_OPERATORE IN (
					SELECT o.ID_OPERATORE 
					FROM OPERATORI o
					where upper(o.LOGIN_OPERATORE) = upper('$sOperatore')
					)
			) and upper(G.NOME) in ('MAGAZZINIERE')");
  
		if ($checkOperatore > 0){
			$querySql .= q{ where upper(V.MAGAZZINO) in (
														/* magazzini dell'operatore */
														SELECT SUBSTR(G.NOME,2) 
														FROM GRUPPI g 
														where G.ID_GRUPPO in (
															SELECT OG.ID_GRUPPO 
															FROM OPERATORI_GRUPPI og 
															WHERE og.ID_OPERATORE IN (
																SELECT o.ID_OPERATORE 
																FROM OPERATORI o
																where upper(o.LOGIN_OPERATORE) = upper('%s')
																)
														) and (upper(G.NOME) like 'M1%' or  upper(G.NOME) like 'M2%'))},$sOperatore;
		}
	}
	
	eval {
		my  $stmt = $self->_db->create_prepare(
			$querySql
		);
	
		$res->{data} = $stmt->fetchall_hashref();
	};
	if( $@ ) {
		$res->{ErrDesc} = $@;
		$res->{ErrCode} = -1;
	}
	
		return $res;

}


1;
