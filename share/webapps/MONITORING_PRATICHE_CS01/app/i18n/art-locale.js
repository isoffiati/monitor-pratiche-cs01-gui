'use strict';

var dateFormat = {
  Date: "dd/MM/yyyy",
  Time: "HH:mm:ss",
  DateTime: "dd/MM/yyyy HH:mm:ss",
  TimeShort: "HH:mm",
  DateTimeShort: "dd/MM/yyyy HH:mm"
};

angular.module('art.locale', [])
// convert date to str
.filter('artDateToStrDate', function($filter) {
	return function(date) {
		return $filter('date')(date, dateFormat.Date);
	}
})
.filter('artDateToStrTime', function($filter) {
	return function(date) {
		return $filter('date')(date, dateFormat.Time);
	}
})
.filter('artDateToStrDateTime', function($filter) {
	return function(date) {
		return $filter('date')(date, dateFormat.DateTime);
	}
})
.filter('artDateToStrTimeShort', function($filter) {
	return function(date) {
		return $filter('date')(date, dateFormat.TimeShort);
	}
})
.filter('artDateToStrDateTimeShort', function($filter) {
	return function(date) {
		return $filter('date')(date, dateFormat.DateTimeShort);
	}
})
// get date formats
.factory('ArtDateTimeFormats', function() {
	return {
		Date: function() {
			return dateFormat.Date
		},
		Time: function() {
			return dateFormat.Time
		},
		DateTime: function() {
			return dateFormat.DateTime
		}
	}
})
;
