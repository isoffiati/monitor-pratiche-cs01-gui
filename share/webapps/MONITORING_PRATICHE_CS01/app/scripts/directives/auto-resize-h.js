'use strict';
	/**
	 * New node file
	 */
	/**
	 * This directive listens for windows resize and fires a broadcast event 
	 */
angular.module('guiMonitorPraticheCs01')

	.directive('windowResizeListener', function($window) {
		return {
			link : function(scope) {

				function onResize(/* e */) {
					// Fires a broadcast event 
					scope.$broadcast('windowResizeListener::resize');
				}

				function onCleanUp(/* e */) {
					// Fires a broadcast event 
					scope.$broadcast('windowResizeListener::cleanUp');
				}
				
				function cleanUp() {
					angular.element($window).off('resize', onResize);
				}

				angular.element($window).on('resize', onResize);
				angular.element($window).on('cleanUp', onCleanUp);
				scope.$on('$destroy', cleanUp);
			}
		};
	})	

	/**
	 * This directive allow a <div> to be vertically resized according to window resize 
	 */
	.directive('autoResizeH', function($window) {
		return {
			restrict: 'A',
			scope: {
				autoResizeHEvent: '@',	// Tells which event listen for (mandatory)
				autoResizeHDelta: '@'	// Tells the amount of window height to reserve 
			},
			link: function (scope, element) {
			
				// Event to listen for (mandatory)
				if (!scope.autoResizeHEvent) {
					return;
				}
				
				$window.addEventListener('resize', function() {
					resizeElement();
				});
				
				// Parses delta parameter to use while resizing 
				var autoResizeHDelta = scope.autoResizeHDelta ? parseInt(scope.autoResizeHDelta) : 0; 

				// Looks for current window size
				var w = angular.element($window);
				var getWindowDimensions = function () {
					return { 'h': w[0].innerHeight, 'w': w[0].innerWidth };
				};

				// Defines listener for broadcasting event
				scope.$on(scope.autoResizeHEvent, function() {
					resizeElement();
				});
				
				// Resizes element according to 
				var resizeElement = function() {
					var d = getWindowDimensions();
					scope.dimension = d;
					element[0].style.height =	parseInt(scope.dimension.h - autoResizeHDelta) + 'px';
				};
			
				// Initialize element size at load time
				resizeElement();
				
				//** On resize
				$window.onresize = function () {
					resizeElement();
					//scope.$apply();
				};
			}
		};
	});
	