'use strict';

/**
 * @ngdoc service
 * @name advlogwsServices
 * @description # advlogwsServices.
 */

angular.module('advlogwsServices', [])

	.value('ADVLOGWSCanAccessObj', {})

	.run(function($rootScope, $q, ADVLOGWSCanAccessObj) {
		$rootScope.ADVLOGWS = {
			profilo: {
				gruppi: []
			},
			err: undefined
		};
		ADVLOGWSCanAccessObj.promise = $q.defer();
	})

	.factory('ADVLOGWSConfigService', function($resource, _, CheckAuthInterceptor, $rootScope) {
		return function() {
			return $resource($rootScope.config.AdvlogwsAppService + 'config', {}, {
				get : {
					method : 'GET',
					params : {},
					withCredentials : true,
					interceptor : CheckAuthInterceptor
				}
			}, {
				stripTrailingSlashes : true
			});
		};
	})

	.factory('ADVLOGWSProfile', function($rootScope, ADVLOGWSCanAccessObj) {
		return {
			canAccess: function() {
				return ADVLOGWSCanAccessObj.promise.promise;
			},
			gruppi: function() { return $rootScope.ADVLOGWS.profilo.gruppi; },
			err: function() { return $rootScope.ADVLOGWS.err; }
		};
	})
	
	.factory('ADVLOGWSConfig', function($rootScope, ADVLOGWSCanAccessObj) {
		return {
			set: function(config, err) {
				if(err) {
					ADVLOGWSCanAccessObj.promise.reject('Not Authorized');
					$rootScope.ADVLOGWS.err = err;
					return;
				}
				$rootScope.ADVLOGWS.profilo.gruppi        = config.profilo.gruppi;

				ADVLOGWSCanAccessObj.promise.resolve(true);
			}
		};
	})
;
