'use strict';

/**
 * @ngdoc service
 * @name advlog.advlogProfileServices
 * @description # advlogProfileServices Factory in the advlog.
 */

angular.module('advlogProfileServices', [ 'Utils' ])

		.factory('ProfileService', function($resource, _, CheckAuthInterceptor, $rootScope) {
			return function() {
				return $resource($rootScope.config.AdvlogProfilePage, {}, {
					get : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthInterceptor
					}
				}, {
					stripTrailingSlashes : true
				});
			};
		})

		.factory('ProfileConfig', function($rootScope) {
			return {
				set: function(profile) {
					$rootScope.userProfile = profile;
				}
			};
		})

		.factory('UserProfile', function($rootScope) {
			return {
				idUtente: function() { return $rootScope.userProfile.idUtente; },
				nomeUtente: function() { return $rootScope.userProfile.nomeUtente; },
				nome: function() { return $rootScope.userProfile.nome; },
				cognome: function() { return $rootScope.userProfile.cognome; },
				email: function() { return $rootScope.userProfile.email; },
				telefoni: function() { return $rootScope.userProfile.telefoni; },
				gruppi: function() { return $rootScope.userProfile.gruppi; },
				isAdmin : function() { var ret = false; if($rootScope.userProfile.nomeUtente === 'ROOT'){ret = true;  return ret;} return ret;},
			};
		})

;
