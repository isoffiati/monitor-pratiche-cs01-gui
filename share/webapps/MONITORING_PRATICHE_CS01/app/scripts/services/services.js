'use strict';

angular
.module('guiMonitorPraticheCs01Services', [])
//LISTA V_ADVCSAQ_DEQUEUE_KO
.factory('getListaPraticheKo', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/guiMonitorPraticheCs01/bonificaPratiche/listaPraticheKo/grid', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
//LISTA DELLE PRATICHE ko
.factory('getListaDtaPratiche', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/guiMonitorPraticheCs01/bonificaPratiche/listaDtaPratiche/grid', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
//LISTA DELLE PRATICHE ko
.factory('getListaCodiciCampiCs01', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/guiMonitorPraticheCs01/bonificaPratiche/listaCodiciCampiCs01/grid', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('getListaDtaPolarys', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/guiMonitorPraticheCs01/bonificaPratiche/listaDtaPolarys/grid', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
//LISTA DELLE PRATICHE ko
.factory('getListaBonificaHistory', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/guiMonitorPraticheCs01/bonificaHistory/listaBonificaHistory/grid', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('modificaBody', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/guiMonitorPraticheCs01/bonificaPratiche/action/modificaBody', {}, {
		postData : {
			method : 'POST',
			params : {rollback:UiConfig.rollBack()},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('eliminaDaBody', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/guiMonitorPraticheCs01/bonificaPratiche/action/eliminaDaBody', {}, {
		postData : {
			method : 'POST',
			params : {rollback:UiConfig.rollBack()},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('aggiungiAlBody', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/guiMonitorPraticheCs01/bonificaPratiche/action/aggiungiAlBody', {}, {
		postData : {
			method : 'POST',
			params : {rollback:UiConfig.rollBack()},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('sottomettiHeader', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/guiMonitorPraticheCs01/bonificaPratiche/action/sottomettiHeader', {}, {
		postData : {
			method : 'POST',
			params : {rollback:UiConfig.rollBack()},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('chiusuraHeader', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/guiMonitorPraticheCs01/bonificaPratiche/action/chiusuraHeader', {}, {
		postData : {
			method : 'POST',
			params : {rollback:UiConfig.rollBack()},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('ingaggioToCs01', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/guiMonitorPraticheCs01/bonificaPratiche/action/ingaggioToCs01', {}, {
		postData : {
			method : 'POST',
			params : {rollback:UiConfig.rollBack()},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})



;
