'use strict';

/**
 * @ngdoc service
 * @name advlog.Alerts
 * @description
 * # Alerts
 * Factory in the advlog.
 */

angular.module('Alerts', [])

.config([ 'growlProvider', function(growlProvider) {
	growlProvider.globalTimeToLive({
		success : 3000,
		error : 3000,
		warning : 3000,
		info : 3000
	});
	growlProvider.globalPosition('top-center');
	growlProvider.globalInlineMessages(false);
} ])

.factory('Alert', function(growl,gettextCatalog) {
	var messages = [];
	return {
		fatal: function(o) {
			var config = {
				ttl: -1,
				disableCloseButton: true
			};
			var msg;
			if (angular.isObject(o)) {
				msg = o.message;
				if (o.internalMessage){ msg += '<div ng-if="error.internalMessage"><p><em>'+gettextCatalog.getString('Error detail')+'</em>: ' + o.internalMessage + '</p></div>'; }
				if (o.UUID){ msg += '<div ng-if="error.UUID" class="push-right small uuid">(UUID: <code>' + o.UUID + '</code>)</div>'; }
			} else {
				msg = o;
			}
			growl.error(msg, config);
		},
		error: function(o, config) {
			if (!angular.isObject(config)){ config = { ttl: -1 , enableHtml: true }; }
			var msg;
			if (angular.isObject(o)) {
				msg = o.message;
				if (o.internalMessage){ msg += '<div ng-if="error.internalMessage"><p><em>'+gettextCatalog.getString('Error detail')+'</em>: ' + o.internalMessage + '</p></div>'; }
				if (o.UUID){ msg += '<div ng-if="error.UUID" class="push-right small uuid">(UUID: <code>' + o.UUID + '</code>)</div>'; }
			} else {
				msg = o;
			}
			// config Modify 
			if(config.position){ growl.position = function(){ return config.position; };}
			messages.push(growl.error(msg, config));
			growl.error(msg, config);
		},
		info: function(msg, config) {
			if (!angular.isObject(config)){ config = { ttl: -1 , enableHtml: true}; }
			// config Modify 
			if(config.position){ growl.position = function(){ return config.position; };}
			messages.push(growl.info(msg, config));
			growl.info(msg, config);
		},
		warning: function(o, config) {
			if (!angular.isObject(config)){ config = { ttl: -1 , enableHtml: true}; }
			var msg;
			if(o.message) {
				if(o.message){msg += '<div><p><em>'+gettextCatalog.getString('Error message')+'</em>: ' + o.message + '</p></div>';}
				if (o.internalMessage){ msg += '<div ng-if="error.internalMessage"><p><em>'+gettextCatalog.getString('Error detail')+'</em>: ' + o.internalMessage + '</p></div>'; }
				if (o.UUID){ msg += '<div ng-if="error.UUID" class="push-right small uuid">(UUID: <code>' + o.UUID + '</code>)</div>'; }
			}
			
			// config Modify 
			if(config.position){ growl.position = function(){ return config.position; };}
			messages.push(growl.warning(o, config));
			growl.warning(o, config);
		},
		success: function(msg, config) {
			if (!angular.isObject(config)){ config = { ttl: -1 , enableHtml: true}; }
			// config Modify 
			if(config.position){ growl.position = function(){ return config.position; };}
			messages.push(growl.success(msg, config));
			growl.success(msg, config);
		},
		deleteMessage: function(){
			for(var i=0; i<messages.length; i++) {
				if(angular.isDefined(messages[i])) {
					messages[i].destroy();
				}
			}
			messages = [];
		}
	};
})

;
