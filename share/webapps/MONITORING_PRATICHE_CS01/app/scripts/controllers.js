'use strict';


function guiMonitorPraticheCs01Ctrl($state,$stateParams,$rootScope,$q,$scope,$interval,$location,$window,
	$uibModal, $log, $document,Alert,UserProfile,UiConfig,_,$filter,$timeout,getListaPraticheKo){
	
	$scope.alert = Alert;
	$scope.firtTest = 'Primo Test';
	$scope.classCollapse = 'sidebar-collapse';
	$scope.classMenuOpen = 'menu-open';
	$scope.propDisplay   = 'block';
	$scope.stateParams = $stateParams;
	$scope.keyIn = $scope.stateParams.key || 'MAIN';
	$scope.linkLogOut = UiConfig.AdvlogHomePage() + 'principale.html';
	
	$scope.isAbilitatoGui = false;
	
	$scope.getProfiloGUI = function(){
		
			console.log('getProfiloGUI');

			var arrSup = $filter('filter')($scope.profilo.gruppi, 'PROJECT MANAGER');
			console.log(arrSup);
			if (arrSup.length > 0) {
				$scope.profilo.PM = true;
			} else {
				$scope.profilo.PM = false;
			}

			arrSup = $filter('filter')($scope.profilo.gruppi, 'ASSISTENTE TECNICO');
			console.log(arrSup);
			if (arrSup.length > 0) {
				$scope.profilo.AT = true;
			} else {
				$scope.profilo.AT = false;
			}

			arrSup = $filter('filter')($scope.profilo.gruppi, 'SQUADRA');
			console.log(arrSup);
			if (arrSup.length > 0) {
				$scope.profilo.SQ = true;
			} else {
				$scope.profilo.SQ = false;
			}

			arrSup = $filter('filter')($scope.profilo.gruppi, 'MAGAZZINIERE');
			console.log(arrSup);
			if (arrSup.length > 0) {
				$scope.profilo.MG = true;
			} else {
				$scope.profilo.MG = false;
			}
	};
	
	$scope.profilo = {
		PM:null,
		CM:null,
		MG:null,
		SQ:null,
		AT:null,
		admin:UserProfile.isAdmin(),
		magazzini:null,
		quadruple:null,
		nomeOperatore:UserProfile.nomeUtente(),
		gruppi: UserProfile.gruppi()
	};

	console.log('$scope.profilo');
	console.log($scope.profilo);
	

	$scope.collapseMenu = function(){
		if ($scope.classCollapse === 'sidebar-collapse'){
			$scope.classCollapse = ''; 
		}
		else{
			$scope.classCollapse = 'sidebar-collapse';
		}
	};

	$scope.openTreeView = function(){
		if ($scope.classMenuOpen === 'menu-open'){
			$scope.classMenuOpen = '';
			$scope.propDisplay   = 'none';
		}else{
			$scope.classMenuOpen = 'menu-open';
			$scope.propDisplay   = 'block';
		}
	};

	// config Main
	$scope.configMainMenu = {
			BONIFICA_PRATICHE : {
				divider		: true,
				name			: 'BONIFICA_PRATICHE',
				icon			: 'fa-file-text-o',
				icon2			: '',
				colorB		: '#4863A0',
				color			: 'bg-steelBlueTrasfS2S color-palette',
				label			: 'BONIFICA PRATICHE',
				subLabel	:	'Visualizzazione e bonifica delle Pratiche CS01 in elaborazione KO',
				url				:	'bonificaPratiche',
				visibled	:	true,
				disabled	:	false,
			},
			BONIFICA_HISTORY : {
				divider		: true,
				name			: 'BONIFICA_HISTORY',
				icon			: 'fa-calendar-check-o',
				icon2			: '',
				colorB		: '#4863A0',
				color			: 'bg-steelBlueTrasfS2S color-palette',
				label			: 'BONIFICA HISTORY',
				subLabel	:	'Visualizzazione delle bonifiche eseguite',
				url				:	'bonificaHistory',
				visibled	:	true,
				disabled	:	false,
			},
			MODIFICA_CONFIGURAZIONI : {
				divider		: true,
				name			: 'MODIFICA_CONFIGURAZIONI',
				icon			: 'fa-cogs',
				icon2			: '',
				colorB		: '#6d84b4',
				color			: 'bg-light-blue color-palette',
				label			: 'CONFIGURAZIONI',
				subLabel	:	'Visualizzazione e modifica delle configurazioni attive',
				url				:	'modificaConfigurazioni',
				visibled	:	true,
				disabled	:	false,
			}
	};

	
	

	
	

	$scope.showMenu = {
			BONIFICA_PRATICHE						: $scope.keyIn === 'BONIFICA_PRATICHE' ? true : false,
			MODIFICA_CONFIGURAZIONI			: $scope.keyIn === 'MODIFICA_CONFIGURAZIONI' ? true : false,
			BONIFICA_HISTORY						: $scope.keyIn === 'BONIFICA_HISTORY' ? true : false,
			MAIN												: $scope.keyIn === 'MAIN' ? true : false
	};
	
	
	
	
	
	/*gestione della visibilita */
	
	// se sei ROOT vedi e fai tutto 
	if ($scope.profilo.nomeOperatore !== ''){
		
		$scope.isAbilitatoGui																		= true;
		$scope.showMenu.MAIN																		= true;
		$scope.showMenu.BONIFICA_PRATICHE										= true;
		$scope.configMainMenu.BONIFICA_PRATICHE.visibled			= true;
		$scope.configMainMenu.BONIFICA_PRATICHE.disabled			= false;
		
		$scope.showMenu.MODIFICA_CONFIGURAZIONI											= true;
		$scope.configMainMenu.MODIFICA_CONFIGURAZIONI.visibled				= true;
		$scope.configMainMenu.MODIFICA_CONFIGURAZIONI.disabled				= false;

		$scope.showMenu.BONIFICA_HISTORY											= true;
		$scope.configMainMenu.BONIFICA_HISTORY.visibled				= true;
		$scope.configMainMenu.BONIFICA_HISTORY.disabled				= false;

	}
	else {

		$scope.isAbilitatoGui																		= false;
		$scope.showMenu.MAIN																		= false;
		$scope.showMenu.BONIFICA_PRATICHE										= false;
		$scope.configMainMenu.BONIFICA_PRATICHE.visibled			= false;
		$scope.configMainMenu.BONIFICA_PRATICHE.disabled			= true;
		$scope.showMenu.MODIFICA_CONFIGURAZIONI											= false;
		$scope.configMainMenu.MODIFICA_CONFIGURAZIONI.visibled				= false;
		$scope.configMainMenu.MODIFICA_CONFIGURAZIONI.disabled				= true;
		$scope.showMenu.BONIFICA_HISTORY											= false;
		$scope.configMainMenu.BONIFICA_HISTORY.visibled				= false;
		$scope.configMainMenu.BONIFICA_HISTORY.disabled				= true;		
		
		$state.go('notAuthorized');
	}






	// gestione rinnovo sessione
	$interval(function () {
		getListaPraticheKo.getData({}).$promise.then(function(result) {
			console.log('Rinnovo la sessione dopo 1 min');
		},
		function(err) {
			console.log('Errore Rinnovo la sessione dopo 1 min');
		});
	}, 300000);







	
}


angular.module('guiMonitorPraticheCs01')
	.controller('guiMonitorPraticheCs01Ctrl', guiMonitorPraticheCs01Ctrl);
