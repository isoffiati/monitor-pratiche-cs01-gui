'use strict';



function gsModificaCfgCtrl($rootScope,$q,$scope,$stateParams,$interval,$location,
	$window,$uibModal, $log, $document,$filter,_,gettextCatalog,
	$timeout, filterNumbers, filterDate,Alert,
	getListaTrasferimenti,$state){
	
	/* global XLSX */
	
	$scope.stateParamsUI				= $stateParams;
	
	
	console.log($stateParams,$scope.configMainMenu);
	
	// controllo del profilo
	
	if ((!$scope.configMainMenu[$scope.stateParamsUI.section].visibled) && ($scope.configMainMenu[$scope.stateParamsUI.section].disabled)){
		console.log('Non Sei abilitato');
		$state.go('notAuthorized');
	}
	
	$scope.coloreUI 						= $scope.configMainMenu[$scope.stateParamsUI.section].colorB;
	$scope.coloreColUI					= $scope.configMainMenu[$scope.stateParamsUI.section].color;
	$scope.labelUI							= $scope.configMainMenu[$scope.stateParamsUI.section].label;
	$scope.subLabelUI						= $scope.configMainMenu[$scope.stateParamsUI.section].subLabel;
	$scope.iconUI								= $scope.configMainMenu[$scope.stateParamsUI.section].icon;
	$scope.icon2UI							= $scope.configMainMenu[$scope.stateParamsUI.section].icon2;
	$scope.profiloUI 						= $scope.$parent.profilo;
	$scope.alert 								= Alert;
	$scope.itemSelectedUI 			= [];
	$scope.actionSelectedUI			= ''; 

	
		$scope.buttonFunctionGrid = {
		modificaRecord 	: {
			icon:'glyphicon glyphicon-pencil'			,
			label:'Gestisci Materiale'	,
			visible: true,
			classe : 'btn '+$scope.coloreColUI+' btn-xs',
			azione :'SELEZIONA_MATERIALE'
		}
	};
	
	
	console.log('gruppi: ',$scope.profiloUI.gruppi);
	
	$scope.magazziniOperatore = $filter('filter')($scope.profilo.gruppi, 'M2');
	
	console.log('magazziniOperatore: ',$scope.magazziniOperatore);
	
	$scope.filterMagazOp = '';
		
		angular.forEach($scope.magazziniOperatore, function(magazOp) {
			
			$scope.filterMagazOp += magazOp.replace('M','')+',';
			
		});	
	
	$scope.configGridUI		= {
		FUNCTION: {
		    divider: false,
		    name: 'FUNCTION',
		    label: '',
		    visible: true,
		    orderable: false,
		    filtered: false,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '125px',
		    icon: 'fa-cogs',
		    enabled: false,
				enableFilter : false,
		    type:'string'
		  },
		  ID_ATTIVITA: {
		    divider: false,
		    name: 'ID_ATTIVITA',
		    label: 'ID ATTIVITA',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'number'
		  },
		  OPERATORE_APERTURA: {
		    divider: false,
		    name: 'OPERATORE_APERTURA',
		    label: 'OPERATORE APERTURA',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '200px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  STATO: {
		    divider: true,
		    name: 'STATO',
		    label: 'STATO',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: '',
		    enabled: true,
				enableFilter : true,
		    type:'string'
		  },
		  DATA_APERTURA: {
		    divider: false,
		    name: 'DATA_APERTURA',
		    label: 'DATA APERTURA',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '120px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'date'
		  },
		  DATA_ULTIMA_AZIONE: {
		    divider: false,
		    name: 'DATA_ULTIMA_AZIONE',
		    label: 'DATA ULTIMA AZIONE',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
				enableFilter : true,
		    type:'date'
		  },
		  MAGAZZINO: {
		    divider: false,
		    name: 'MAGAZZINO',
		    label: 'MAGAZZINO',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  CODICE_SIRTI: {
		    divider: false,
		    name: 'CODICE_SIRTI',
		    label: 'CODICE SIRTI',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '120px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  PART_NUMBER: {
		    divider: false,
		    name: 'PART_NUMBER',
		    label: 'PART NUMBER',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '160px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  QUANTITA: {
		    divider: false,
		    name: 'QUANTITA',
		    label: 'QUANTITA',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'number'
		  },
		  TIPO_MATERIALE: {
		    divider: false,
		    name: 'TIPO_MATERIALE',
		    label: 'TIPO MATERIALE',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '160px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  RUOLO_SQUADRA: {
		    divider: false,
		    name: 'RUOLO_SQUADRA',
		    label: 'RUOLO SQUADRA',
		    visible: false,
		    orderable: false,
		    filtered: false,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: false,
				enableFilter : false,
		    type:'string'
		  },
		  NOME_SQUADRA: {
		    divider: false,
		    name: 'NOME_SQUADRA',
		    label: 'SQUADRA',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '200px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  PROGETTO: {
		    divider: true,
		    name: 'PROGETTO',
		    label: 'PROGETTO',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
				enableFilter : true,
		    type:'string'
		  },
		  SOTTO_PROGETTO: {
		    divider: true,
		    name: 'SOTTO_PROGETTO',
		    label: 'SOTTO PROGETTO',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
				enableFilter : true,
		    type:'string'
		  },
		  PROPRIETA: {
		    divider: true,
		    name: 'PROPRIETA',
		    label: 'PROPRIETA\'',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
				enableFilter : true,
		    type:'string'
		  },
		  CLIENTE_FINALE: {
		    divider: true,
		    name: 'CLIENTE_FINALE',
		    label: 'CLIENTE FINALE',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
				enableFilter : true,
		    type:'string'
		  }

	};
	
	
	
	$scope.options = {
				orderBy : ['ID_ATTIVITA'],
				global : '',
				searchSingleColumn : true
		};
	
	$scope.currentPage  = 1;
	$scope.itemsPerPage = 10;
	$scope.orderByField = $scope.configGridUI[$scope.options.orderBy[0]].name;
	$scope.reverseSort  = false;
	$scope.maxSizePager = 6;
	$scope.nuovoNumero = 10;
	$scope.mostraLoaderGrid = true;
	
	// composto data header e data
	$scope.objectDati = {
			header:$scope.configGridUI,
			data:[]
	};
	
	
	//##################################### ATTIVAZIONE FILTRI ###################################//
	$scope.filterText = {}; // mappa chiave/valore con campo/valore da filtrare
	$scope.filterOperator = {}; // mappa chiave/valore con campo/operatore di filtro da applicare
	$scope.sortDirection = {}; // mappa chiave/valore con campo/direzione di ordinamento
	$scope.sortOrderArray = []; // array con l'elenco ordinato dei campi da ordinare
	$scope.sortOrderMap = {}; // mappa chiave/valore con i campi/posizione da ordinare. parallelo a $scope.sortOrderArray ma serve per essere passato
								// alla direttiva sirti-column-sort-and-filter nell'attributo sort-position in modo che non debba essere calcolato
	
	$scope.filterOrOrderApplied = false;
	
	function isFilterOrOrderApplied() {
		var isFiltered = false;
		var isSorted = false;
		_.each($scope.filterOperator, function(value) {
			if(!_.isUndefined(value)) {
				isFiltered = true;
			}
		});
		_.each($scope.sortOrderMap, function(value) {
			if(!_.isUndefined(value)) {
				isSorted = true;
			}
		});
		return isFiltered || isSorted;
	}
	
	$scope.$watch('filterOperator', function() {
		$scope.filterOrOrderApplied = isFilterOrOrderApplied();
	});
	
	$scope.$watch('sortOrderMap', function() {
		$scope.filterOrOrderApplied = isFilterOrOrderApplied();
	});
	
	// funzione che recepisce l'applica della direttiva sirti-column-sort-and-filter
	$scope.applyFilterAndSort = function(field, filterText, filterOperator, sortDirection) {
		if(field === 'id' && filterText && !filterText.match(/^\d+$/)) {
			$scope.alert.warning(gettextCatalog.getString('Activity Id must be a number'));
			return;
		}
	
		$scope.filterText[field] = filterText;
		$scope.filterOperator[field] = filterOperator;
		$scope.sortDirection[field] = sortDirection;
		if($scope.sortDirection[field]) {
			// la direzione di sort del campo è valorizzata e la aggiungo all'array se non ancora presente
			if(!_.contains($scope.sortOrderArray, field)) {
				$scope.sortOrderArray.push(field);
			}
		} else {
			// ...altrimenti elimino il campo dall'ordinamento
			$scope.sortOrderArray = _.without($scope.sortOrderArray, field);
		}
		// costruisco la mappa a partire dall'array di ordinamento (vedi nota sull'oggetto $scope.sortOrderMap)
		$scope.sortOrderMap = {};
		var i = 1;
		_.each($scope.sortOrderArray, function(value) {
			$scope.sortOrderMap[value] = i++;
		});
		// invoco il reload della tabella
		$scope.getGrid();
	};
	
	
	// pulisco filtri
	$scope.clearFiltersAndOrdering = function() {
		$scope.filterText = {};
		$scope.filterOperator = {};
		$scope.sortDirection = {};
		$scope.sortOrderArray = [];
		$scope.sortOrderMap = {};
		$scope.options.global = '';
		// invoco il reload della tabella
		$scope.getGrid();
	};
	
	//###################################### FINE PREPARAZIONE RICERCA #####################################//
	
	$scope.getGrid = function() {
		//var withFilter = [];//['permitsAreaId'];
		//var withOutSorter = [];
		//var withAutocompleate = [];//['macroTaskSubCategory','macroTaskCategory'];
		//var hidden = [];//['Pfp','Pop','projectId','customerId','contractId','buildingId','macroTaskType'];
		var oldTemp = [];
		var temp = [];
		
		$scope.mostraLoaderGrid = true;
	

	
			// #################### DEFAULT IN BASE PROFILO AZIONI ###################
		var pCall = [];
		// #################### DEFAULT IN BASE PROFILO AZIONI  FINE###################
		
		
		/* gestione del filtro per escludere ROOT */
		
		if (!$scope.profiloUI.admin){
			if ( angular.isUndefined(pCall.filter_MAGAZZINO_in) ) {
					pCall.filter_MAGAZZINO_in = $scope.filterMagazOp;
			}
		} 	

		if ( angular.isUndefined(pCall.filter_STATO_equals) ) {
				pCall.filter_STATO_equals = 'APERTA';
		} 

		// parametri in URI di ricerca su BKEND		
		var params = {
				sort:[],
				pCall : pCall
		};
		
		//################################# POPOLAMENTO FILTRI ##################################//
		
		var localFilter = {};
		// definisco i parametri di filtro (testo e operatore)
		_.each($scope.filterOperator, function(value, key) {
			if($scope.filterOperator[key] && $scope.filterText[key]) {
				// solo in questo caso setto il parametro dello skip a 0 
				// in modo da avere sempre la ripartenza dallo 0esimo
				//params.skip = 0;
				params[key + '_' + $scope.filterOperator[key]] = $scope.filterText[key];
				// popolo i filtri locali
				localFilter[key] = $scope.filterText[key];

				// remap per far funzionare l'equal
				if($scope.filterOperator[key] === 'equal'){$scope.filterOperator[key] = 'equals';}
				// aggiungo i parametri di ricerca a pCall
				pCall['filter_'+ key + '_' + $scope.filterOperator[key]] = $scope.filterText[key];
				
				if(key === 'status') {
					// per lo status sostituisco gli spazi con _ secondo la convenzione di chiamare gli stati senza spazi e con le parole divise da _
					params[key + '_' + $scope.filterOperator[key]] = params[key + '_' + $scope.filterOperator[key]].replace(/ /g, '_');
				}
			}
		});
	
		// definisco l'ordine di sort
		_.each($scope.sortOrderArray, function(key) {
			if($scope.sortDirection[key]) {
				params.sort.push(($scope.sortDirection[key] === 'ASC' ? '' : '-') + key);
			}
		});
	
		// se non c'è alcun ordinamento ordino per id DESC
		// agisco in questo punto per evitare che la direttiva sirti-column-sort-and-filter evidenzi tale ordinamento
		if(params.sort.length === 0) {	
			params.sort = $scope.options.filterBy;
		}
		// ############################################ FINE POPOLAMENTO ########################################// 
		//getListaRichiestePm.getData(pCall).$promise.then(function(result) {
		getListaTrasferimenti.getData(pCall).$promise.then(function(result) {
			temp = result.data.results;
			$scope.totalItems = result.data.count;
			
			$scope.objectDati.data = temp; 
			$scope.objectDati = filterNumbers(filterDate($scope.objectDati));

			// ###################################### FILTRO LOCALE ATTIVO ##########################//
			// FIXME filtri locali in caso attivati i filtri sui campi 
			$scope.objectDati.data = $filter('orderBy')($scope.objectDati.data = $filter('filter')((temp || oldTemp),localFilter),params.sort);
			// ###################################### FILTRO LOCALE ATTIVO ##########################//
			$scope.data = $scope.objectDati.data;
			
			$scope.data = _.each($scope.objectDati.data,function(row){
				$scope.showFunction = true;
				row.hiddenButton = false;
				row.mostraModificaRecord		= true;
				$scope.buttonFunctionGrid.modificaRecord.visible=true;
				
				
				
			});
			
			$scope.mostraLoaderGrid = false;
			

		},
		function(err) {
			$scope.mostraLoaderGrid = false;
			$scope.alert.error(err.data);
		});
		
	
		
	};
	
	$scope.getGrid();	


			// funzione di export tabella
$scope.exportToExcel = function(){
	
	// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
	
	$scope.mostraLoaderUiModal = true;
	
	var confHeader	= {};
	var dataValue		=	{};
	var nomeFile 		= '';
	var nomeSheet		= '';
	
	
		
	nomeSheet = 'MATERIALI_DA_CONSEGNARE';
	
	nomeFile	= $scope.configMainMenu[$scope.stateParamsUI.section].name;
	
	angular.forEach($scope.configGridUI, function(value,key) {
		console.log(key);
		console.log(value);
		if ((key !=='FUNCTION') && value.visible === true){
			confHeader[key] = {name : value.name, id : value.name, label : value.name, visible : true, enabled : false, type:'string' };
		}
	
	});
	
	dataValue = $scope.data;
	
	// call services macrotasks


		var data = [];
		// faccio logica per definire il set dati da printare
		var dateNow = new Date();
		dateNow = $filter('date')(dateNow,'yyyyMMddHHmmss');
	
		var obj = {
				header	:	confHeader,
				data		:	dataValue
		};
		// ciclo su di esso
		obj = filterNumbers(filterDate(obj));
		console.log(obj);
		_.each(obj.data, function(row) {
			// costruzione nuovo oggetto per la stampa
			var printObj = {};
			_.each(row,function(val,key){
				// restituisce la row modificata con le sole chiavi con visibilita a true
				//console.log(confHeader[key]);
				
				if((confHeader[key]) && (confHeader[key].visible)){ printObj[confHeader[key].label] = val; }
			});
			// add all'array 
			data.push(printObj);
		});

		var ws =  XLSX.utils.json_to_sheet (data);
		// costruttore obj excel
		var wb =  XLSX.utils.book_new ();
		// appendo gli elementi al file
		XLSX.utils.book_append_sheet (wb, ws, nomeSheet );
		// scrivo il file
		XLSX.writeFile(wb, nomeFile+'_'+dateNow+'.xlsx');

		$scope.mostraLoaderUiModal = false;
		
		// jscs:enable requireCamelCaseOrUpperCaseIdentifiers

};	
	
	
	/* cambia pagina */ 
	$scope.pageChanged = function() {
		var startPos = ($scope.currentPage - 1) * $scope.itemsPerPage;
		return startPos;
	};
	
	/* cfg dei campi edit */
	
	$scope.configEditUI = {
		
	};	
	
	
	$scope.configButtonUI = {

	};
	
	/* configurazione della modale invocata dalla UI */
	$scope.configMaterialiDominioModalUI =	{
		SELECTED: {
		    name: 'SELECTED',
		    label: '',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    icon:'fa fa-cogs',
		    action: '',
		    classe:	{'width': '100px', 'min-width': '100px', 'max-width': '100px', 'white-space': 'normal','text-align': 'center','vertical-align':'middle'},
				filter :	{
										visible	:	true,
										tipo		:	'ICON',
										icon		: 'fa fa-ban',
										paddingBottom : '10px',
										tpPosition		: 'bottom-left',
										tooltip				: 'Selezionare almeno un materiale'
								}
		  },
		  ID_ATTIVITA: {
		    name: 'ID_ATTIVITA',
		    label: 'ID ATTIVITA',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
		    classe:{'width': '80px', 'min-width': '80px', 'max-width': '80px', 'white-space': 'normal', 'text-align': 'center','vertical-align':'middle'},
				filter :	{
										visible	:	true,
										tipo		:	'NUMBER',
										paddingBottom : '5px'
								}
		  },
		  CODICE_SIRTI: {
		    name: 'CODICE_SIRTI',
		    label: 'CODICE SIRTI',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
		    classe:{'width': '80px', 'min-width': '80px', 'max-width': '80px', 'white-space': 'normal', 'text-align': 'center','vertical-align':'middle'},
				filter :	{
										visible	:	true,
										tipo		:	'TEXT',
										paddingBottom : '5px'
								}
		  },
		  PART_NUMBER: {
		    name: 'PART_NUMBER',
		    label: 'PART NUMBER',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
		    classe:{'width': '100px', 'min-width': '100px', 'max-width': '100px', 'white-space': 'normal', 'text-align': 'center','vertical-align':'middle'},
				filter :	{
										visible	:	true,
										tipo		:	'TEXT',
										paddingBottom : '5px'
								}
		  },
		  ID_SIRTI: {
		    name: 'ID_SIRTI',
		    label: 'ID SIRTI',
		    visibleInSummary: false,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
		    classe:{'width': '80px', 'min-width': '80px', 'max-width': '80px', 'white-space': 'normal', 'text-align': 'center','vertical-align':'middle'},
				filter :	{
										visible	:	false,
										tipo		:	'NUMBER',
										paddingBottom : '5px'
								},
				type	: 'EDIT_SPED'
		  },
		  SERIAL_NUMBER: {
		    name: 'SERIAL_NUMBER',
		    label: 'SERIAL NUMBER',
		    visibleInSummary: false,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
		    classe:{'width': '120px', 'min-width': '120px', 'max-width': '120px', 'white-space': 'normal', 'text-align': 'center','vertical-align':'middle'},
				filter :	{
										visible	:	false,
										tipo		:	'TEXT',
										paddingBottom : '5px'
								},
				type	: 'EDIT_SPED'
		  },
		  MOTIVO_ANNULLAMENTO: {
		    name: 'MOTIVO_ANNULLAMENTO',
		    label: 'MOTIVO ANNULLAMENTO',
		    visibleInSummary: false,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
		    classe:{'width': '200px', 'min-width': '200px', 'max-width': '200px', 'white-space': 'normal', 'text-align': 'center','vertical-align':'middle'},
				filter :	{
										visible	:	false,
										tipo		:	'TEXT',
										paddingBottom : '5px'
								},
				type	: 'EDIT_ANNULLA'
		  },
		  QUANTITA: {
		    name: 'QUANTITA',
		    label: 'QTA',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
		    classe:{'width': '50px', 'min-width': '50px', 'max-width': '50px', 'white-space': 'normal', 'text-align': 'center','vertical-align':'middle'},
				filter :	{
										visible	:	true,
										tipo		:	'NUMBER',
										paddingBottom : '5px'
								}
		  },
		  TIPO_MATERIALE: {
		    name: 'TIPO_MATERIALE',
		    label: 'TIPO MATERIALE',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
		    classe:{'width': '100px', 'min-width': '100px', 'max-width': '100px', 'white-space': 'normal', 'text-align': 'center','vertical-align':'middle'},
				filter :	{
										visible	:	true,
										tipo		:	'TEXT',
										paddingBottom : '5px'
								}
		  },
		  MAGAZZINO: {
		    name: 'MAGAZZINO',
		    label: 'MAGAZZINO',
		    visibleInSummary: false,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
		    classe:{'width': '80px', 'min-width': '80px', 'max-width': '80px', 'white-space': 'normal','text-align': 'center','vertical-align':'middle'},
				filter :	{
										visible	:	true,
										tipo		:	'TEXT',
										paddingBottom : '5px'
								}
		  },
		  RUOLO_SQUADRA: {
		    name: 'RUOLO_SQUADRA',
		    label: 'RUOLO SQUADRA',
		    visibleInSummary: false,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
		    classe:{'width': '80px', 'min-width': '80px', 'max-width': '80px', 'white-space': 'normal','text-align': 'center','vertical-align':'middle'},
				filter :	{
										visible	:	true,
										tipo		:	'SELECT',
										paddingBottom : '5px'
								}
		  },
			CONSEGNA_SQUADRA_TRASFERIMENTO: {
				name: 'CONSEGNA_SQUADRA_TRASFERIMENTO',
				label: 'Conferma Consegna',
				visibleInSummary: false,
				visibleInVerify: false,
				visibleInAction: true,
				action: 'CONSEGNA_SQUADRA_TRASFERIMENTO',
				isMassiva: true,
				modNoSpedAz : '',
				icon : 'fa fa-check',
				disabled	: true
			},
			ANNULLA_TRASFERIMENTO: {
				name: 'ANNULLA_TRASFERIMENTO',
				label: 'Annulla Trasferimento',
				visibleInSummary: false,
				visibleInVerify: false,
				visibleInAction: true,
				action: 'ANNULLA_TRASFERIMENTO',
				isMassiva: true,
				modNoSpedAz : '',
				icon	: 'fa fa-check',
				disabled	: false
			}

	};
	
	
	/* apro la modale con i parametri specifici */
	
	$scope.openModalForm = function (itemSelected,uiAction){
		
		console.log('itemSelectedUI:');
		console.log(itemSelected);
		console.log('actionSelectedUI: '+uiAction);
		
		
		/* UI MODALE - Inizio */
		
		var $ctrl = this;
		
		$ctrl.animationsEnabled = true;
		
		$ctrl.open = function (size, parentSelector,itemSelected,uiChiamante,uiConfig,tab) {
			var modalInstance = $uibModal.open({
				animation: $ctrl.animationsEnabled,
				backdrop:false,
				keyboard:false,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: './views/common/modalMaterialiDominio.html',
				controller: 'modalMaterialiDominioCtrl',
				controllerAs: '$ctrl',
				size: size,
				//appendTo: parentElem,
				resolve: {
						uiChiamante: function () {
							return uiChiamante;
						},
						itemSelected: function () {
							return itemSelected;
						},
						uiConfig: function () {
							return uiConfig;
						},
						tab: function () {
							return tab;
						}
				}
			});
		
			modalInstance.result.then(function (retObj) {
				console.log(retObj);
				$scope.getGrid();
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};
		
		
		$ctrl.open('fullscreen',null,itemSelected,$scope.configMainMenu[$scope.stateParamsUI.section],$scope.configMaterialiDominioModalUI,'');

	};


}


angular.module('guiMonitorPraticheCs01').controller('gsModificaCfgCtrl', gsModificaCfgCtrl);