
'use strict';

/* Controller della Form Modale che verra' chiamata da tutte le UI */

function  modalGestitaUfficioCtrl($scope,$rootScope,$uibModalInstance,itemSelected,uiChiamante,uiConfig,tab,Alert,
	$window, _,$timeout,filterDate,filterNumbers,$filter,$q,chiusuraHeader){
	/*jshint validthis: true */

	// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
	
	$scope.$ctrl = this;

	
	var oriCtrl={};
	
	$scope.$ctrl.alert = Alert;
	
	$scope.$ctrl.mostraLoaderUiModal = false;

	$scope.$ctrl.item										= itemSelected;
	$scope.$ctrl.chiamante							= uiChiamante;
	$scope.$ctrl.config									= uiConfig;
	$scope.$ctrl.cssSmall								= 'small';
	$scope.$ctrl.tab										= tab;
	$scope.$ctrl.disabledConferma				= true;
	$scope.$ctrl.disabledPulisci				= false;
	$scope.$ctrl.actionSelected					= '';
	$scope.$ctrl.numeroEditPerRiga			= 'col-md-6';
	
	/* gestisco la forzatura dell'header */
	$scope.$ctrl.item.ESITO							= 'OK-FORZATO';
	$scope.$ctrl.item.CAUSALE						= '';
	$scope.$ctrl.item.DATA_ELABORAZIONE	= '';
	
	// chiusra della modale
	$scope.$ctrl.cancel = function () {

		var retObj = {
		};
		
		$uibModalInstance.close(retObj);
		//$uibModalInstance.dismiss('cancel');
	};
	
	// procedura che gestisce il salva chiusura d'ufficio sull'Header
	$scope.$ctrl.chiusuraHeaderPratica = function(){
		//console.log('chiusuraHeaderPratica');
		
		$scope.$ctrl.mostraLoaderUiModal = true;
		
		// oggetto parametri da mandare alla funzione di chiusuraHeader
		
		var paramsCall = {
			ID									:	$scope.$ctrl.item.ID						,
			ID_SOURCE						:	$scope.$ctrl.item.ID_SOURCE		,
			ID_POLARYS					:	$scope.$ctrl.item.ID_POLARYS		,
			SOURCE							:	$scope.$ctrl.item.SOURCE				,
			TARGET							:	$scope.$ctrl.item.TARGET				, 
			TIPOINTERVENTO			:	$scope.$ctrl.item.TIPOINTERVENTO,
			EVENTO							:	$scope.$ctrl.item.EVENTO,
			ESITO								:	$scope.$ctrl.item.ESITO,
			CAUSALE							:	$scope.$ctrl.item.CAUSALE,
			DATA_ELABORAZIONE		:	$scope.$ctrl.item.DATA_ELABORAZIONE
		};
		
		console.log('chiusuraHeader => paramsCall: ',paramsCall);
		
		
		// gestisco la chiamata
		
		var deferred = $q.defer();
		var urlCalls = [];
		
		
		/* richiamo la funzione di movimentazione */
		var esito =	chiusuraHeader.postData(paramsCall).$promise.then(function(resultCall) {
		
			return	{
								ESITO 			: resultCall.data.ESITO,
								DESC_ESITO 	:	resultCall.data.DESC_ESITO
							};
		
		},function(){
				return {
								ESITO 			: 'KO',
								DESC_ESITO 	:	'Errore interno alla funzione chiusuraHeader'
							};
		});
					
		urlCalls.push(esito);
		
		/* gestisco gli esiti */
				
		$q.all(urlCalls)
		.then(
			function(results) {
				deferred.resolve();
		
				console.log(results);
				
				$scope.$ctrl.mostraLoaderUiModal = false;
				
				angular.forEach(results, function(esitoChiamata) {
					
					if (esitoChiamata.ESITO === 'OK'){
						$scope.$ctrl.alert.success(esitoChiamata.DESC_ESITO, { ttl: 10000 });
						//$scope.$ctrl.cancel();
					}
					else{
						$scope.$ctrl.alert.error(esitoChiamata.DESC_ESITO, { ttl: 10000 });
					}
				});

		},
		function(errors) {
			deferred.reject(errors);
			$scope.$ctrl.mostraLoaderUiModal = false;
		},
		function(updates) {
			deferred.update(updates);
			$scope.$ctrl.mostraLoaderUiModal = false;
		});
	
	};

	
// jscs:enable requireCamelCaseOrUpperCaseIdentifiers	
}

angular.module('guiMonitorPraticheCs01').controller('modalGestitaUfficioCtrl', modalGestitaUfficioCtrl);