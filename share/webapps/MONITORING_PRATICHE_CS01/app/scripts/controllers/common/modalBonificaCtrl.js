
'use strict';

/* Controller della Form Modale che verra' chiamata da tutte le UI */

function  modalBonificaCtrl($scope,$rootScope,$uibModalInstance,itemSelected,uiChiamante,uiConfig,tab,Alert,
	$window, _,$timeout,filterDate,filterNumbers,$filter,getListaDtaPratiche,getListaCodiciCampiCs01,uibDateParser,
	$q,	modificaBody,eliminaDaBody,aggiungiAlBody,sottomettiHeader,getListaDtaPolarys,ingaggioToCs01){
	/*jshint validthis: true */

	// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
	
	$scope.$ctrl = this;

	
	var oriCtrl={};
	
	$scope.$ctrl.alert = Alert;
	
	$scope.$ctrl.mostraLoaderUiModal = false;
	$scope.$ctrl.mostraLoaderAutocomplete = false;
	
	$scope.$ctrl.item										= itemSelected;
	$scope.$ctrl.chiamante							= uiChiamante;
	$scope.$ctrl.config									= uiConfig;
	$scope.$ctrl.cssSmall								= 'small';
	$scope.$ctrl.tab										= tab;
	$scope.$ctrl.disabledConferma				= true;
	$scope.$ctrl.disabledPulisci				= false;
	$scope.$ctrl.actionSelected					= '';
	$scope.$ctrl.numeroEditPerRiga			= 'col-md-6';
	
	$scope.$ctrl.abilitaBtnSottometti		= true;
	$scope.$ctrl.abilitaBtnAggiungi			= true;
	$scope.$ctrl.abilitaBtnIngaggio			= false;
	
	$scope.$ctrl.globalBodySearch				= '';
	
	// tipi di DATE cfg in CS01
	$scope.$ctrl.tipiDataCs01						= [
		{
			chiave				:	'DATA_1::D',
			formato				:	'DD/MM/YYYY',
			parseFormat		:	'dd/MM/yyyy'
		},
		{
			chiave				:	'DATA_1::H',
			formato				:	'HH24:MI:SS',
			parseFormat		:	'HH:mm:ss'
		},
		{
			chiave				:	'DATA_2::D',
			formato				:	'DD/MM/YYYY',
			parseFormat		:	'dd/MM/yyyy'
		},
		{
			chiave				:	'DATA_2::H',
			formato				:	'HH24:MI:SS',
			parseFormat		:	'HH:mm:ss'
		},
		{
			chiave				:	'TIMS01::D',
			formato				:	'DD/MM/YYYY',
			parseFormat		:	'dd/MM/yyyy'
		},
		{
			chiave				:	'TIMS01::H',
			formato				:	'HH24:MI:SS',
			parseFormat		:	'HH:mm:ss'
		},
		{
			chiave				:	'TIMS02::D',
			formato				:	'DD/MM/YYYY',
			parseFormat		:	'dd/MM/yyyy'
		},
		{
			chiave				:	'TIMS02::H',
			formato				:	'HH24:MI:SS',
			parseFormat		:	'HH:mm:ss'
		},
		{
			chiave				:	'TIMS01',
			formato				:	'DD/MM/YYYY HH24:MI:SS',
			parseFormat		:	'dd/MM/yyyy HH:mm:ss'
		},
		{
			chiave				:	'TIMS02',
			formato				:	'DD/MM/YYYY HH24:MI:SS',
			parseFormat		:	'dd/MM/yyyy HH:mm:ss'
		}
	];

	
	//console.log(uiChiamante);
	//console.log(itemSelected);
	
	
	// chiusra della modale
	$scope.$ctrl.cancel = function () {

		var retObj = {
		};
		
		$uibModalInstance.close(retObj);
		//$uibModalInstance.dismiss('cancel');
	};

	// get dati di una pratica
	$scope.$ctrl.getDtaPratica = function() {
		
		$scope.$ctrl.abilitaBtnIngaggio	= false;
		
		$scope.$ctrl.mostraLoaderUiModal = true;
		
		
		console.log('ESITO: ',$scope.$ctrl.item.ESITO);
		console.log('CAUSALE: ',$scope.$ctrl.item.CAUSALE);
		console.log('ID_POLARYS: ',$scope.$ctrl.item.ID_POLARYS);
		console.log('ID_SOURCE: ',$scope.$ctrl.item.ID_SOURCE);
		
		/* verifico se mi trovo nela ESITO = KO-VALORE per eseguire il parser della CAUSALE */
		if ($scope.$ctrl.item.ESITO === 'KO-VALORE'){
			
			/* eseguo lo split della CAUSALE */
			var arrCausale = $scope.$ctrl.item.CAUSALE.split('=');
			
			console.log('arrCausale: ',arrCausale);
			
			/* definisco la chiave e il valore */
			var keyCs01		=	arrCausale[0].trim();
			var valueCs01	=	arrCausale[1].trim();
			
			console.log(keyCs01,valueCs01);
			
			/* eseguo la chiamata su V_HERMES_CONF per recuperare il dta di polarys */
			
			var filterGrid = {};
			
			/* costruisco il filtro per interrogare la grid V_HERMES_CONF */
			
			filterGrid = {
				filter_CODICE_CAMPO_CS01_equals				:	keyCs01,
				filter_TIPO_INTERVENTO_AMELIA_equals	:	$scope.$ctrl.item.TIPOINTERVENTO,
				filter_EVENTO_equals									:	$scope.$ctrl.item.EVENTO
			};
			
			/* eseguo la chiamata */
			getListaCodiciCampiCs01.getData(filterGrid).$promise.then(function(result) {
				
				console.log(result.data.results);
				
				if ((result.data.results) && (result.data.results.length === 1)){
					
					var keyDtaPolarys = result.data.results[0].CODICE_CAMPO_ART;
					
					console.log('keyDtaPolarys: ',keyDtaPolarys);
					
					/* costruisco il filtro per interrogare la grid V_HERMES_VALORI_DTA */

					var filterGridPol = {
						filter_ID_ATTIVITA_equals						:	$scope.$ctrl.item.ID_SOURCE,
						filter_TIPO_DATO_TECNICO_ART_equals	:	keyDtaPolarys,
						filter_AZIONE_ART_equals						:	$scope.$ctrl.item.EVENTO
						/*filter_VALORE_equals								:	$scope.$ctrl.item.EVENTO*/
					};
					
					getListaDtaPolarys.getData(filterGridPol).$promise.then(function(resultPol) {
						$scope.$ctrl.mostraLoaderUiModal = false;
						console.log(resultPol.data.results);
						
						/* se il chiave esiste */
						if ((resultPol.data.results) && (resultPol.data.results.length === 1)){
							
							/* controllo se il valore e' stato modificato */
							if (resultPol.data.results[0].VALORE.toUpperCase() !== valueCs01.toUpperCase()){
								$scope.$ctrl.mostraLoaderUiModal = false;
								$scope.$ctrl.getBody();
								$scope.$ctrl.alert.success('Valore = '+resultPol.data.results[0].VALORE.toUpperCase()+' modificato su Polarys per CODICE_CAMPO_CS01 = '+keyCs01+'. Eseguire nuovamente l\'Ingaggio a CS01', { ttl: 10000 });
								$scope.$ctrl.abilitaBtnIngaggio	= true;
							} else {
								$scope.$ctrl.mostraLoaderUiModal = false;
								$scope.$ctrl.getBody();
                $scope.$ctrl.globalBodySearch=valueCs01.toUpperCase();
								$scope.$ctrl.alert.warning('Valore = '+resultPol.data.results[0].VALORE.toUpperCase()+' non modificato su Polarys per CODICE_CAMPO_CS01 = '+keyCs01, { ttl: 10000 });
							}
							
						} else {
							$scope.$ctrl.mostraLoaderUiModal = false;
							$scope.$ctrl.getBody();
							$scope.$ctrl.alert.error('Nessuna DTA = '+keyDtaPolarys+' per l\'id_polarys = '+$scope.$ctrl.item.ID_POLARYS+' e id_source = '+$scope.$ctrl.item.ID_SOURCE, { ttl: 10000 });
						}

					});
					
					
					
				} else {
					$scope.$ctrl.mostraLoaderUiModal = false;
					$scope.$ctrl.getBody();
					$scope.$ctrl.alert.error('Nessuna configurazione trovata per il CODICE_CAMPO_CS01 = '+keyCs01, { ttl: 10000 });
					
				}
			
			});
			
		} else {
			$scope.$ctrl.getBody();
		}
	};
	
	$scope.$ctrl.getBody = function() {
		
		$scope.$ctrl.mostraLoaderUiModal = true;
		
		var filterGridBody = {};
		
		filterGridBody = {
			filter_ID_equals				: $scope.$ctrl.item.ID
		};
		
		getListaDtaPratiche.getData(filterGridBody).$promise.then(function(result) {
			
			_.each(result.data.results , function(item){
				
				//ingetto il colore del box
				
				item.classColore 	= 'box-primary';
				item.sfondoColore	=	'none';
				
				//ingetto  isModifyKey e isModifyValue x tutti
				item.isModifyKey		= false;
				item.isModifyValue	= false;
				
				//ingetto sempre i vecchi valori di NOME_CAMPO e VALORE_CAMPO
				item.NOME_CAMPO_OLD		= item.NOME_CAMPO;
				item.VALORE_CAMPO_OLD	= item.VALORE_CAMPO;
				
				
				// controllo se il NOME_CAMPO e' una data
				_.each( _.where($scope.$ctrl.tipiDataCs01 , { chiave : item.NOME_CAMPO } ) , function(tipoData){
					
					//console.log(tipoData);
					
					// ingetto il formatoData solo per le date
					
					if(tipoData){
						item.VALORE_CAMPO	= uibDateParser.parse(item.VALORE_CAMPO, tipoData.parseFormat);
						item.parseFormat	= tipoData.parseFormat;
						item.formatoLabel	= tipoData.formato;
						
					}
					
				});
		
			});
			
			$scope.$ctrl.dtaPratica = result.data.results;
			//result.data.count;
			$scope.$ctrl.mostraLoaderUiModal = false;
		},
		function(err) {
			$scope.$ctrl.mostraLoaderUiModal = false;
			//console.log(err);
		});
		
	};

	// leggo la pratica passata
	
	$scope.$ctrl.getDtaPratica();
		

	// cerco il nome campo nell'autcomplete specifico
	$scope.$ctrl.cercaNomeCampoCs01 = function(valoreCs01){
		
		$scope.$ctrl.mostraLoaderAutocomplete = true;
		
		var filterGrid = {};
		
		filterGrid = {
			filter_CODICE_CAMPO_CS01_begins				:	valoreCs01,
			filter_TIPO_INTERVENTO_AMELIA_equals	:	$scope.$ctrl.item.TIPOINTERVENTO,
			filter_EVENTO_equals									:	$scope.$ctrl.item.EVENTO
		};

		
		//$scope.$ctrl.mostraLoaderUiModal = true;
		return getListaCodiciCampiCs01.getData(filterGrid).$promise.then(function(result) {
			
			$scope.$ctrl.mostraLoaderAutocomplete = false;
			
			console.log(result.data.results);
			var contaDoppi = 0;
			var iDoppi = [];
			_.each($scope.$ctrl.dtaPratica , function(item){
				console.log(item.NOME_CAMPO_OLD)
				
				_.each( _.where(result.data.results , { CODICE_CAMPO_CS01 : ""+item.NOME_CAMPO_OLD.toUpperCase() } ) , function(campo){
				 console.log(campo)	;
				 if(campo){
					console.log('esiste',item.NOME_CAMPO_OLD);
					//result.data.results.pop();
					iDoppi.push(campo);
					contaDoppi++;
				 } 
				});
			});
			
			if(iDoppi.length > 0){
				_.each(result.data.results , function(campo){
				result.data.results = _.difference(result.data.results,iDoppi);
				});
			}
			
			////console.log(result.data.results);
			//$scope.$ctrl.mostraLoaderUiModal = false;
			if (!result.data.results){
				
				// controllo se esiste tra quelli già presenti
				
				_.each( _.where($scope.$ctrl.dtaPratica , { NOME_CAMPO_OLD : ""+valoreCs01.toUpperCase() } ) , function(campo){
				 console.log(campo)	;
				 if(campo){
					console.log('esiste',campo.NOME_CAMPO_OLD);
					contaDoppi++;
				 }

				});

				if(contaDoppi > 0){
					$scope.$ctrl.alert.error('Il valore digitato e\' gia\' presente tra quelli associati alla pratica', { ttl: 10000 });
					$scope.$ctrl.mostraLoaderAutocomplete = true;
				} else {
					$scope.$ctrl.alert.error('Il valore digitato non e\' presente tra quelli disponibili. Verificare che sia un nuovo valore configurato su CS01', { ttl: 10000 });
					$scope.$ctrl.mostraLoaderAutocomplete = false;
				}
				

			} else {
			
				if(result.data.results.length === 0 && contaDoppi > 0){
					$scope.$ctrl.alert.error('Il valore digitato e\' gia\' presente tra quelli associati alla pratica', { ttl: 10000 });
					$scope.$ctrl.mostraLoaderAutocomplete = true;
				}
				
			}
			

			return result.data.results;
		

		});	

	};
	
	// procedura che gestisce il valore selezionato di cs01 dall'autocomplete
	$scope.$ctrl.controllaCampoCs01Selected =  function ($item,itemPratica){
		
		console.log($item);
		
		var isDate = false;
		
		itemPratica.NOME_CAMPO					= $item.CODICE_CAMPO_CS01;
		itemPratica.TDTA_ART						=	$item.CODICE_CAMPO_ART;
		itemPratica.ETICHETTA_TDTA_ART	=	$item.ETICHETTA_TDTA_ART;
		itemPratica.TIPO_HCONF					=	$item.TIPO_HCONF;
		
		// controllo se il NOME_CAMPO e' una data
		_.each( _.where($scope.$ctrl.tipiDataCs01 , { chiave : $item.CODICE_CAMPO_CS01 } ) , function(tipoData){
			
			//console.log(tipoData);
			
			// ingetto il formatoData solo per le date
			itemPratica.errore = false;
			
			if(tipoData){
				
				//itemPratica.VALORE_CAMPO	= '';
				itemPratica.parseFormat		= tipoData.parseFormat;
				itemPratica.formatoLabel	= tipoData.formato;
				isDate = true;
				// modifico la chiave, ma non il valore
				itemPratica.isModifyKey		= true;
				itemPratica.isModifyValue	= false;
				itemPratica.errore = true;
			}
			
		});
		
		if (!isDate){
			// modifico la chiave, ma non il valore
			itemPratica.isModifyKey		= true;
			itemPratica.isModifyValue	= false;
		}
		
		//console.log(itemPratica);
		
		// disabilito i pulsanti sotto
		$scope.$ctrl.abilitaBtnSottometti		= false;
		$scope.$ctrl.abilitaBtnAggiungi			= false;
		
	};
	
	// procedura che controlla se l'edit e' cambiato
	$scope.$ctrl.controllaCampo = function(item,editType){
		
		item.errore = false;
		//console.log('controllaCampo: ',item);
		

			
			//console.log('dentro');
		
			
			
			
			var isDate = false;
			// controllo se il NOME_CAMPO e' una data

			_.each( _.where($scope.$ctrl.tipiDataCs01 , { chiave : item.NOME_CAMPO } ) , function(tipoData){
				
				//console.log(tipoData);
				
				// ingetto il formatoData solo per le date
				
				if(tipoData){
					
					//console.log('qua');
					
					isDate = true;
					
					// se veine modificata l'etichetta, allora resetto sempre il  valore
					if (editType === 'CS01'){
						//console.log('qui');
						item.VALORE_CAMPO = '';
						item.isModifyValue	= false;
						item.isModifyKey		= true;
						item.parseFormat	= tipoData.parseFormat;
						item.formatoLabel	= tipoData.formato;
						item.errore = true;
					} else {
						//console.log('quo');
						var checkData	= uibDateParser.parse(item.VALORE_CAMPO, tipoData.parseFormat);
						if (!checkData) {
							//console.log('errore');
							item.errore = true;
							item.isModifyValue	= false;
						} else {
							item.isModifyValue	= true;
						}
						
					}
					
					
	
					
	
				}
				
			});
				
			// e' una stringa modificata
			if (!isDate){
				if (editType === 'CS01'){
					item.VALORE_CAMPO = '';
					item.isModifyValue	= false;
					item.isModifyKey		= true;
					item.errore = true;
				} else {
					if ((item.VALORE_CAMPO !== '') && (item.VALORE_CAMPO !== undefined) && (item.VALORE_CAMPO !== null )){
						if ((item.NOME_CAMPO !== '') && (item.NOME_CAMPO !== undefined) && (item.NOME_CAMPO !== null )){
							item.isModifyValue	= true;
						} else {
							item.errore = true;
						}						
					} else {
						item.errore = true;
					}
				}
			}

		

		//console.log(item);
		
		// disabilito i pulsanti sotto
		$scope.$ctrl.abilitaBtnSottometti		= false;
		$scope.$ctrl.abilitaBtnAggiungi			= false;	

	};
	
	// procedura che gestisce l'annullamento della modifica su un campo
	$scope.$ctrl.anullaCampo = function(){
		
		// resetto i pulsanti sotto
		$scope.$ctrl.abilitaBtnSottometti		= true;
		$scope.$ctrl.abilitaBtnAggiungi			= true;
		$scope.$ctrl.globalBodySearch				= '';
		
		$scope.$ctrl.getDtaPratica();
	};
	
	// porcedura che gestisce la modifica
	$scope.$ctrl.modificaCampo = function(item){
		
		$scope.$ctrl.mostraLoaderUiModal = true;
		
		// oggetto parametri da mandare alla funzione di modifica
		
		var paramsCall = {
			ID									:	item.ID						,
			ID_SOURCE						:	$scope.$ctrl.item.ID_SOURCE		,
			ID_POLARYS					:	$scope.$ctrl.item.ID_POLARYS		,
			TDTA_ART						:	item.TDTA_ART		,
			TIPO_HCONF					:	item.TIPO_HCONF		,
			SOURCE							:	item.SOURCE				,
			TARGET							:	item.TARGET				, 
			NOME_CAMPO					:	''		, 
			VALORE_CAMPO				:	''	,
			TIPOINTERVENTO			:	item.TIPOINTERVENTO,
			EVENTO							:	item.EVENTO,
			NOME_CAMPO_OLD			:	item.NOME_CAMPO_OLD, 
			VALORE_CAMPO_OLD		:	item.VALORE_CAMPO_OLD	
		};
		
		if (!angular.isObject(item.NOME_CAMPO)){
			paramsCall.NOME_CAMPO = item.NOME_CAMPO.toUpperCase();
		} else {
			paramsCall.VALORE_CAMPO	= item.NOME_CAMPO.item.CODICE_CAMPO_CS01;
		}
		
	  if(item.parseFormat){
			paramsCall.VALORE_CAMPO	=	$filter('date')(item.VALORE_CAMPO, item.parseFormat);
		} else {
			paramsCall.VALORE_CAMPO	= item.VALORE_CAMPO.toUpperCase();
		}
		
		
		console.log('modificaCampo => paramsCall: ',paramsCall);
		
		
		// gestisco la chiamata
		
		var deferred = $q.defer();
		var urlCalls = [];
		
		
		/* richiamo la funzione di movimentazione */
		var esito =	modificaBody.postData(paramsCall).$promise.then(function(resultCall) {
		
			return	{
								ESITO 			: resultCall.data.ESITO,
								DESC_ESITO 	:	resultCall.data.DESC_ESITO
							};
		
		},function(){
				return {
								ESITO 			: 'KO',
								DESC_ESITO 	:	'Errore interno alla funzione modificaBody'
							};
		});
					
		urlCalls.push(esito);
		
		/* gestisco gli esiti */
				
		$q.all(urlCalls)
		.then(
			function(results) {
				deferred.resolve();
		
				console.log(results);
				
				$scope.$ctrl.mostraLoaderUiModal = false;
				
				angular.forEach(results, function(esitoChiamata) {
					
					if (esitoChiamata.ESITO === 'OK'){
						$scope.$ctrl.alert.success(esitoChiamata.DESC_ESITO, { ttl: 10000 });
						$scope.$ctrl.anullaCampo();

					}
					else{
						$scope.$ctrl.alert.error(esitoChiamata.DESC_ESITO, { ttl: 10000 });
					}
				});

		},
		function(errors) {
			deferred.reject(errors);
			$scope.$ctrl.mostraLoaderUiModal = false;
		},
		function(updates) {
			deferred.update(updates);
			$scope.$ctrl.mostraLoaderUiModal = false;
		});				

	};
	
	// porcedura che gestisce la cancellazione
	$scope.$ctrl.eliminaCampo = function(item){

		$scope.$ctrl.mostraLoaderUiModal = true;
		
		// oggetto parametri da mandare alla funzione di elimina
		
		var paramsCall = {
			ID									:	item.ID						,
			ID_SOURCE						:	$scope.$ctrl.item.ID_SOURCE		,
			ID_POLARYS					:	$scope.$ctrl.item.ID_POLARYS		,
			TDTA_ART						:	item.TDTA_ART		,
			TIPO_HCONF					:	item.TIPO_HCONF		,
			SOURCE							:	item.SOURCE				,
			TARGET							:	item.TARGET				, 
			NOME_CAMPO					:	item.NOME_CAMPO,  
			VALORE_CAMPO				:	item.VALORE_CAMPO,
			TIPOINTERVENTO			:	item.TIPOINTERVENTO,
			EVENTO							:	item.EVENTO,
			NOME_CAMPO_OLD			:	item.NOME_CAMPO_OLD, 
			VALORE_CAMPO_OLD		:	item.VALORE_CAMPO_OLD	
		};
		
	
		console.log('eliminaCampo => paramsCall: ',paramsCall);
		
		
		// gestisco la chiamata
		
		var deferred = $q.defer();
		var urlCalls = [];
		
		
		/* richiamo la funzione di movimentazione */
		var esito =	eliminaDaBody.postData(paramsCall).$promise.then(function(resultCall) {
		
			return	{
								ESITO 			: resultCall.data.ESITO,
								DESC_ESITO 	:	resultCall.data.DESC_ESITO
							};
		
		},function(){
				return {
								ESITO 			: 'KO',
								DESC_ESITO 	:	'Errore interno alla funzione eliminaDaBody'
							};
		});
					
		urlCalls.push(esito);
		
		/* gestisco gli esiti */
				
		$q.all(urlCalls)
		.then(
			function(results) {
				deferred.resolve();
		
				console.log(results);
				
				$scope.$ctrl.mostraLoaderUiModal = false;
				
				angular.forEach(results, function(esitoChiamata) {
					
					if (esitoChiamata.ESITO === 'OK'){
						$scope.$ctrl.alert.success(esitoChiamata.DESC_ESITO, { ttl: 10000 });
						$scope.$ctrl.anullaCampo();
					}
					else{
						$scope.$ctrl.alert.error(esitoChiamata.DESC_ESITO, { ttl: 10000 });
					}
				});

		},
		function(errors) {
			deferred.reject(errors);
			$scope.$ctrl.mostraLoaderUiModal = false;
		},
		function(updates) {
			deferred.update(updates);
			$scope.$ctrl.mostraLoaderUiModal = false;
		});

	};

	// procedura che permette di creare un nuovo item
	$scope.$ctrl.creaNuovoItem = function(){
		//console.log('creaNuovoItem');
		
		//console.log('lista dta prima: ',$scope.$ctrl.dtaPratica);
		
		var newItem = {
			ID										:	$scope.$ctrl.dtaPratica[0].ID						,
			SOURCE								:	$scope.$ctrl.dtaPratica[0].SOURCE				,
			TARGET								:	$scope.$ctrl.dtaPratica[0].TARGET				, 
			NOME_CAMPO						:	''		, 
			VALORE_CAMPO					:	''	,
			TIPOINTERVENTO				:	$scope.$ctrl.dtaPratica[0].TIPOINTERVENTO,
			EVENTO								:	$scope.$ctrl.dtaPratica[0].EVENTO				,
			TDTA_ART							:	'NUOVO CAMPO',
			ETICHETTA_TDTA_ART		: 'NUOVO_CAMPO',
			NOME_CAMPO_OLD				:	'',
			VALORE_CAMPO_OLD			:	'',
			isModifyKey						:	false,
			isModifyValue					:	false,
			errore								: true,
			isNewEditBox					: true,
			classColore						:	'box-danger',
			sfondoColore					: 'lightyellow'
		};
		
		//console.log('newItem: ',newItem);
				
		$scope.$ctrl.dtaPratica.push(newItem);
		
		// disabilito i pulsanti sotto
		$scope.$ctrl.abilitaBtnSottometti		= false;
		$scope.$ctrl.abilitaBtnAggiungi			= false;
		
		//console.log('lista dta dopo: ',$scope.$ctrl.dtaPratica);
		
		$timeout(function() {
			angular.element('#btn_salva_add_nuovo_campo').focus();
			angular.element('#txt_dta_NUOVO_CAMPO_b').focus();
			angular.element('#txt_dta_NUOVO_CAMPO_a').focus();
		});
	};
	
	
	// porcedura che gestisce l'aggiunta di un nuovo campo
	$scope.$ctrl.aggiungiCampo = function(item){

		$scope.$ctrl.mostraLoaderUiModal = true;
		
		// oggetto parametri da mandare alla funzione di aggiungi
		
		var paramsCall = {
			ID									:	item.ID						,
			ID_SOURCE						:	$scope.$ctrl.item.ID_SOURCE		,
			ID_POLARYS					:	$scope.$ctrl.item.ID_POLARYS		,
			TDTA_ART						:	item.TDTA_ART		,
			TIPO_HCONF					:	item.TIPO_HCONF		,
			SOURCE							:	item.SOURCE				,
			TARGET							:	item.TARGET				, 
			NOME_CAMPO					:	''		, 
			VALORE_CAMPO				:	''	,
			TIPOINTERVENTO			:	item.TIPOINTERVENTO,
			EVENTO							:	item.EVENTO,
			NOME_CAMPO_OLD			:	'', 
			VALORE_CAMPO_OLD		:	''	
		};
		
		if (!angular.isObject(item.NOME_CAMPO)){
			paramsCall.NOME_CAMPO = item.NOME_CAMPO.toUpperCase();
		} else {
			paramsCall.VALORE_CAMPO	= item.NOME_CAMPO.item.CODICE_CAMPO_CS01;
		}
		
		if(item.parseFormat){
			paramsCall.VALORE_CAMPO	=	$filter('date')(item.VALORE_CAMPO, item.parseFormat);
		} else {
			paramsCall.VALORE_CAMPO	= item.VALORE_CAMPO.toUpperCase();
		}
		
	
		console.log('aggiungiCampo => paramsCall: ',paramsCall);
		
		
		// gestisco la chiamata
		
		var deferred = $q.defer();
		var urlCalls = [];
		
		
		/* richiamo la funzione di movimentazione */
		var esito =	aggiungiAlBody.postData(paramsCall).$promise.then(function(resultCall) {
		
			return	{
								ESITO 			: resultCall.data.ESITO,
								DESC_ESITO 	:	resultCall.data.DESC_ESITO
							};
		
		},function(){
				return {
								ESITO 			: 'KO',
								DESC_ESITO 	:	'Errore interno alla funzione aggiungiAlBody'
							};
		});
					
		urlCalls.push(esito);
		
		/* gestisco gli esiti */
				
		$q.all(urlCalls)
		.then(
			function(results) {
				deferred.resolve();
		
				console.log(results);
				
				$scope.$ctrl.mostraLoaderUiModal = false;
				
				angular.forEach(results, function(esitoChiamata) {
					
					if (esitoChiamata.ESITO === 'OK'){
						$scope.$ctrl.alert.success(esitoChiamata.DESC_ESITO, { ttl: 10000 });
						$scope.$ctrl.anullaCampo();
					}
					else{
						$scope.$ctrl.alert.error(esitoChiamata.DESC_ESITO, { ttl: 10000 });
					}
				});

		},
		function(errors) {
			deferred.reject(errors);
			$scope.$ctrl.mostraLoaderUiModal = false;
		},
		function(updates) {
			deferred.update(updates);
			$scope.$ctrl.mostraLoaderUiModal = false;
		});

	};
	
	
	
	
	
	// procedura che permette di sottomettere la pratica
	$scope.$ctrl.sottomettiHeaderPratica = function(){
		//console.log('sottomettiHeaderPratica');
		
		$scope.$ctrl.mostraLoaderUiModal = true;
		
		// oggetto parametri da mandare alla funzione di sottomettiHeader
		
		var paramsCall = {
			ID									:	$scope.$ctrl.item.ID						,
			ID_SOURCE						:	$scope.$ctrl.item.ID_SOURCE		,
			ID_POLARYS					:	$scope.$ctrl.item.ID_POLARYS		,
			SOURCE							:	$scope.$ctrl.item.SOURCE				,
			TARGET							:	$scope.$ctrl.item.TARGET				, 
			TIPOINTERVENTO			:	$scope.$ctrl.item.TIPOINTERVENTO,
			EVENTO							:	$scope.$ctrl.item.EVENTO				,
			STATO_PARTENZA			:	$scope.$ctrl.item.STATOPARTENZA,
			CHIAMANTE						:	'SOTTOMETTI'
		};
		
		console.log('sottomettiHeader => paramsCall: ',paramsCall);
		
		
		// gestisco la chiamata
		
		var deferred = $q.defer();
		var urlCalls = [];
		
		
		/* richiamo la funzione di movimentazione */
		var esito =	sottomettiHeader.postData(paramsCall).$promise.then(function(resultCall) {
		
			return	{
								ESITO 			: resultCall.data.ESITO,
								DESC_ESITO 	:	resultCall.data.DESC_ESITO
							};
		
		},function(){
				return {
								ESITO 			: 'KO',
								DESC_ESITO 	:	'Errore interno alla funzione sottomettiHeader'
							};
		});
					
		urlCalls.push(esito);
		
		/* gestisco gli esiti */
				
		$q.all(urlCalls)
		.then(
			function(results) {
				deferred.resolve();
		
				console.log(results);
				
				$scope.$ctrl.mostraLoaderUiModal = false;
				
				angular.forEach(results, function(esitoChiamata) {
					
					if (esitoChiamata.ESITO === 'OK'){
						$scope.$ctrl.alert.success(esitoChiamata.DESC_ESITO, { ttl: 10000 });
						$scope.$ctrl.anullaCampo();
					}
					else{
						$scope.$ctrl.alert.error(esitoChiamata.DESC_ESITO, { ttl: 10000 });
					}
				});

		},
		function(errors) {
			deferred.reject(errors);
			$scope.$ctrl.mostraLoaderUiModal = false;
		},
		function(updates) {
			deferred.update(updates);
			$scope.$ctrl.mostraLoaderUiModal = false;
		});
	
	};
	
	
		// procedura che permette di sottomettere la pratica
	$scope.$ctrl.ingaggioCs01 = function(){
		//console.log('sottomettiHeaderPratica');
		
		$scope.$ctrl.mostraLoaderUiModal = true;
		
		// oggetto parametri da mandare alla funzione di sottomettiHeader
		
		var paramsCall = {
			ID									:	$scope.$ctrl.item.ID						,
			ID_SOURCE						:	$scope.$ctrl.item.ID_SOURCE		,
			ID_POLARYS					:	$scope.$ctrl.item.ID_POLARYS		,
			SOURCE							:	$scope.$ctrl.item.SOURCE				,
			TARGET							:	$scope.$ctrl.item.TARGET				, 
			TIPOINTERVENTO			:	$scope.$ctrl.item.TIPOINTERVENTO,
			EVENTO							:	$scope.$ctrl.item.EVENTO				
		};
		
		console.log('sottomettiHeader => paramsCall: ',paramsCall);
		
		
		// gestisco la chiamata
		
		var deferred = $q.defer();
		var urlCalls = [];
		
		
		/* richiamo la funzione di movimentazione */
		var esito =	ingaggioToCs01.postData(paramsCall).$promise.then(function(resultCall) {
		
			return	{
								ESITO 			: resultCall.data.ESITO,
								DESC_ESITO 	:	resultCall.data.DESC_ESITO
							};
		
		},function(){
				return {
								ESITO 			: 'KO',
								DESC_ESITO 	:	'Errore interno alla funzione ingaggioToCs01'
							};
		});
					
		urlCalls.push(esito);
		
		/* gestisco gli esiti */
				
		$q.all(urlCalls)
		.then(
			function(results) {
				deferred.resolve();
		
				console.log(results);
				
				$scope.$ctrl.mostraLoaderUiModal = false;
				
				angular.forEach(results, function(esitoChiamata) {
					
					if (esitoChiamata.ESITO === 'OK'){
						$scope.$ctrl.alert.success(esitoChiamata.DESC_ESITO, { ttl: 10000 });
						//$scope.$ctrl.anullaCampo();
					}
					else{
						$scope.$ctrl.alert.error(esitoChiamata.DESC_ESITO, { ttl: 10000 });
					}
				});

		},
		function(errors) {
			deferred.reject(errors);
			$scope.$ctrl.mostraLoaderUiModal = false;
		},
		function(updates) {
			deferred.update(updates);
			$scope.$ctrl.mostraLoaderUiModal = false;
		});
	
	};

	

// jscs:enable requireCamelCaseOrUpperCaseIdentifiers	
}

angular.module('guiMonitorPraticheCs01').controller('modalBonificaCtrl', modalBonificaCtrl);
