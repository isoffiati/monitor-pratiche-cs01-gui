'use strict';



function gsBonificaPraticheCtrl($rootScope,$q,$scope,$stateParams,$interval,$location,
	$window,$uibModal, $log, $document,$filter,_,gettextCatalog,
	$timeout, filterNumbers, filterDate,Alert,
	getListaPraticheKo,$state,sottomettiHeader){
	
	/* global XLSX */
	
	$scope.stateParamsUI				= $stateParams;
	
	
	console.log($stateParams,$scope.configMainMenu);
	
	// controllo del profilo
	
	if ((!$scope.configMainMenu[$scope.stateParamsUI.section].visibled) && ($scope.configMainMenu[$scope.stateParamsUI.section].disabled)){
		console.log('Non Sei abilitato');
		$state.go('notAuthorized');
	}
	
	$scope.coloreUI 						= $scope.configMainMenu[$scope.stateParamsUI.section].colorB;
	$scope.coloreColUI					= $scope.configMainMenu[$scope.stateParamsUI.section].color;
	$scope.labelUI							= $scope.configMainMenu[$scope.stateParamsUI.section].label;
	$scope.subLabelUI						= $scope.configMainMenu[$scope.stateParamsUI.section].subLabel;
	$scope.iconUI								= $scope.configMainMenu[$scope.stateParamsUI.section].icon;
	$scope.icon2UI							= $scope.configMainMenu[$scope.stateParamsUI.section].icon2;
	$scope.profiloUI 						= $scope.$parent.profilo;
	$scope.alert 								= Alert;
	$scope.itemSelectedUI 			= [];
	$scope.actionSelectedUI			= ''; 

	
		$scope.buttonFunctionGrid = {
		modificaRecord 	: {
			icon:'glyphicon glyphicon-pencil'			,
			label:'Bonifica Body'	,
			visible: true,
			classe : 'btn '+$scope.coloreColUI+' btn-xs',
			azione :'BONIFICA_PRATICA_SELEZIONATA'
		},
		sottomettiRecord 	: {
			icon:'glyphicon glyphicon-share-alt'			,
			label:'Sottometti'	,
			visible: true,
			classe : 'btn '+$scope.coloreColUI+' btn-xs',
			azione :'SOTTOMETTI_PRATICA_SELEZIONATA'
		},
		sottomettiStato 	: {
			icon:'fa fa-share-alt'			,
			label:'Sottometti Stato'	,
			visible: true,
			classe : 'btn '+$scope.coloreColUI+' btn-xs',
			azione :'SOTTOMETTI_STATO'
		},
		gestisciRecord 	: {
			icon:'glyphicon glyphicon-tag'			,
			label:'Gestita D\'Ufficio'	,
			visible: true,
			classe : 'btn '+$scope.coloreColUI+' btn-xs',
			azione :'PRATICA_SELEZIONATA_GESTITA'
		}
	};
	
	
	//console.log('gruppi: ',$scope.profiloUI.gruppi);
	

	$scope.configGridUI		= {
		FUNCTION: {
		    divider: false,
		    name: 'FUNCTION',
		    label: '',
		    visible: true,
		    orderable: false,
		    filtered: false,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: 'fa-cogs',
		    enabled: false,
				enableFilter : false,
		    type:'string'
		  },
		  ID: {
		    divider: false,
		    name: 'ID',
		    label: 'ID',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'number'
		  },
			SOURCE: {
				divider: true,
				name: 'SOURCE',
				label: 'SOURCE',
				visible: true,
				orderable: true,
				filtered: true,
				color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
				width: '100px',
				icon: '',
				enabled: false,
				enableFilter : true,
				type:'string'
			},
			STATUS_ALIGNED: {
				divider: true,
				name: 'STATUS_ALIGNED',
				label: 'STATUS ALIGNED',
				visible: true,
				orderable: true,
				filtered: true,
				color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
				width: '150px',
				icon: '',
				enabled: false,
				enableFilter : true,
				type:'string'
			},
		  ID_POLARYS: {
		    divider: false,
		    name: 'ID_POLARYS',
		    label: 'ID POLARYS',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'number'
		  },
		  ID_SOURCE: {
		    divider: false,
		    name: 'ID_SOURCE',
		    label: 'ID SOURCE',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'number'
		  },
			ID_CS01: {
				divider: false,
				name: 'ID_CS01',
				label: 'ID CS01',
				visible: true,
				orderable: true,
				filtered: true,
				color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
				width: '100px',
				icon: '',
				enabled: false,
										enableFilter : true,
				type:'number'
			},
			CONTRATTO_POLARYS: {
				divider: false,
				name: 'CONTRATTO_POLARYS',
				label: 'CONTRATTO',
				visible: false,
				orderable: true,
				filtered: true,
				color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
				width: '150px',
				icon: '',
				enabled: true,
										enableFilter : true,
				type:'string'
			},
			TIPO_INTERVENTO_POLARYS: {
				divider: true,
				name: 'TIPO_INTERVENTO_POLARYS',
				label: 'TIPO INTERVENTO',
				visible: false,
				orderable: true,
				filtered: true,
				color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
				width: '150px',
				icon: '',
				enabled: true,
										enableFilter : true,
				type:'string'
			},
		  TIPOINTERVENTO: {
		    divider: false,
		    name: 'TIPOINTERVENTO',
		    label: 'MACRO ATTIVITA\'',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '120px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  EVENTO: {
		    divider: true,
		    name: 'EVENTO',
		    label: 'EVENTO',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '200px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  DATA_ELABORAZIONE: {
		    divider: false,
		    name: 'DATA_ELABORAZIONE',
		    label: 'DATA ELABORAZIONE',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'date'
		  },
		  ESITO: {
		    divider: false,
		    name: 'ESITO',
		    label: 'ESITO',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  CAUSALE: {
		    divider: false,
		    name: 'CAUSALE',
		    label: 'CAUSALE',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '630px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  }

	};
	
	
	
	$scope.options = {
				orderBy : ['ID'],
				global : '',
				searchSingleColumn : true
		};
	
	$scope.currentPage  = 1;
	$scope.itemsPerPage = 15;
	$scope.orderByField = $scope.configGridUI[$scope.options.orderBy[0]].name;
	$scope.reverseSort  = false;
	$scope.maxSizePager = 6;
	$scope.nuovoNumero = 10;
	$scope.mostraLoaderGrid = true;
	
	// composto data header e data
	$scope.objectDati = {
			header:$scope.configGridUI,
			data:[]
	};
	
	
	//##################################### ATTIVAZIONE FILTRI ###################################//
	$scope.filterText = {}; // mappa chiave/valore con campo/valore da filtrare
	$scope.filterOperator = {}; // mappa chiave/valore con campo/operatore di filtro da applicare
	$scope.sortDirection = {}; // mappa chiave/valore con campo/direzione di ordinamento
	$scope.sortOrderArray = []; // array con l'elenco ordinato dei campi da ordinare
	$scope.sortOrderMap = {}; // mappa chiave/valore con i campi/posizione da ordinare. parallelo a $scope.sortOrderArray ma serve per essere passato
								// alla direttiva sirti-column-sort-and-filter nell'attributo sort-position in modo che non debba essere calcolato
	
	$scope.filterOrOrderApplied = false;
	
	function isFilterOrOrderApplied() {
		var isFiltered = false;
		var isSorted = false;
		_.each($scope.filterOperator, function(value) {
			if(!_.isUndefined(value)) {
				isFiltered = true;
			}
		});
		_.each($scope.sortOrderMap, function(value) {
			if(!_.isUndefined(value)) {
				isSorted = true;
			}
		});
		return isFiltered || isSorted;
	}
	
	$scope.$watch('filterOperator', function() {
		$scope.filterOrOrderApplied = isFilterOrOrderApplied();
	});
	
	$scope.$watch('sortOrderMap', function() {
		$scope.filterOrOrderApplied = isFilterOrOrderApplied();
	});
	
	// funzione che recepisce l'applica della direttiva sirti-column-sort-and-filter
	$scope.applyFilterAndSort = function(field, filterText, filterOperator, sortDirection) {
		if(field === 'id' && filterText && !filterText.match(/^\d+$/)) {
			$scope.alert.warning(gettextCatalog.getString('Activity Id must be a number'));
			return;
		}
	
		$scope.filterText[field] = filterText;
		$scope.filterOperator[field] = filterOperator;
		$scope.sortDirection[field] = sortDirection;
		if($scope.sortDirection[field]) {
			// la direzione di sort del campo è valorizzata e la aggiungo all'array se non ancora presente
			if(!_.contains($scope.sortOrderArray, field)) {
				$scope.sortOrderArray.push(field);
			}
		} else {
			// ...altrimenti elimino il campo dall'ordinamento
			$scope.sortOrderArray = _.without($scope.sortOrderArray, field);
		}
		// costruisco la mappa a partire dall'array di ordinamento (vedi nota sull'oggetto $scope.sortOrderMap)
		$scope.sortOrderMap = {};
		var i = 1;
		_.each($scope.sortOrderArray, function(value) {
			$scope.sortOrderMap[value] = i++;
		});
		// invoco il reload della tabella
		$scope.getGrid();
	};
	
	
	// pulisco filtri
	$scope.clearFiltersAndOrdering = function() {
		$scope.filterText = {};
		$scope.filterOperator = {};
		$scope.sortDirection = {};
		$scope.sortOrderArray = [];
		$scope.sortOrderMap = {};
		$scope.options.global = '';
		// invoco il reload della tabella
		$scope.getGrid();
	};
	
	//###################################### FINE PREPARAZIONE RICERCA #####################################//
	
	$scope.getGrid = function() {
		//var withFilter = [];//['permitsAreaId'];
		//var withOutSorter = [];
		//var withAutocompleate = [];//['macroTaskSubCategory','macroTaskCategory'];
		//var hidden = [];//['Pfp','Pop','projectId','customerId','contractId','buildingId','macroTaskType'];
		var oldTemp = [];
		var temp = [];
		
		$scope.mostraLoaderGrid = true;
	

	
			// #################### DEFAULT IN BASE PROFILO AZIONI ###################
		var pCall = [];
		// #################### DEFAULT IN BASE PROFILO AZIONI  FINE###################
		
		
		/* gestione del filtro per escludere ROOT */
		

		// parametri in URI di ricerca su BKEND		
		var params = {
				sort:[],
				pCall : pCall
		};
		
		//################################# POPOLAMENTO FILTRI ##################################//
		
		var localFilter = {};
		// definisco i parametri di filtro (testo e operatore)
		_.each($scope.filterOperator, function(value, key) {
			if($scope.filterOperator[key] && $scope.filterText[key]) {
				// solo in questo caso setto il parametro dello skip a 0 
				// in modo da avere sempre la ripartenza dallo 0esimo
				//params.skip = 0;
				params[key + '_' + $scope.filterOperator[key]] = $scope.filterText[key];
				// popolo i filtri locali
				localFilter[key] = $scope.filterText[key];

				// remap per far funzionare l'equal
				if($scope.filterOperator[key] === 'equal'){$scope.filterOperator[key] = 'equals';}
				// aggiungo i parametri di ricerca a pCall
				pCall['filter_'+ key + '_' + $scope.filterOperator[key]] = $scope.filterText[key];
				
				if(key === 'status') {
					// per lo status sostituisco gli spazi con _ secondo la convenzione di chiamare gli stati senza spazi e con le parole divise da _
					params[key + '_' + $scope.filterOperator[key]] = params[key + '_' + $scope.filterOperator[key]].replace(/ /g, '_');
				}
			}
		});
	
		// definisco l'ordine di sort
		_.each($scope.sortOrderArray, function(key) {
			if($scope.sortDirection[key]) {
				params.sort.push(($scope.sortDirection[key] === 'ASC' ? '' : '-') + key);
			}
		});
	
		// se non c'è alcun ordinamento ordino per id DESC
		// agisco in questo punto per evitare che la direttiva sirti-column-sort-and-filter evidenzi tale ordinamento
		if(params.sort.length === 0) {	
			params.sort = $scope.options.filterBy;
		}
		// ############################################ FINE POPOLAMENTO ########################################//
		
		pCall = {
			filter_EVENTO_notequals				: 'EVENTO DATA SLA FINE INTERVENT'
		};
		
		//getListaRichiestePm.getData(pCall).$promise.then(function(result) {
		getListaPraticheKo.getData(pCall).$promise.then(function(result) {
			temp = result.data.results;
			$scope.totalItems = result.data.count;
			
			$scope.objectDati.data = temp; 
			$scope.objectDati = filterNumbers(filterDate($scope.objectDati));

			// ###################################### FILTRO LOCALE ATTIVO ##########################//
			// FIXME filtri locali in caso attivati i filtri sui campi 
			$scope.objectDati.data = $filter('orderBy')($scope.objectDati.data = $filter('filter')((temp || oldTemp),localFilter),params.sort);
			// ###################################### FILTRO LOCALE ATTIVO ##########################//
			$scope.data = $scope.objectDati.data;
			
			$scope.data = _.each($scope.objectDati.data,function(row){
				
				$scope.showFunction = true;
				row.hiddenButton = false;
				
				/* PULSANTI DI GESTIONE DELL'HEADER */
				if ((row.LOCKED !== 'Y') && (row.ESITO !== '')){	

					/* mostro i pulsanti coretti in base al SOURCE */
					if (row.SOURCE === 'ART'){
						row.modificaRecord		= true;
						row.sottomettiRecord = true;
						row.sottomettiStato = false;
						row.gestisciRecord = true;
					} else {
						row.modificaRecord		= false;
						row.sottomettiRecord = true;
						row.sottomettiStato = true;
						row.gestisciRecord = true;
					}

				
					

				}
				
				
				
			});
			
			$scope.mostraLoaderGrid = false;
			

		},
		function(err) {
			$scope.mostraLoaderGrid = false;
			$scope.alert.error(err.data);
		});
		
	
		
	};
	
	$scope.getGrid();	


			// funzione di export tabella
$scope.exportToExcel = function(){
	
	// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
	
	$scope.mostraLoaderUiModal = true;
	
	var confHeader	= {};
	var dataValue		=	{};
	var nomeFile 		= '';
	var nomeSheet		= '';
	
	
		
	nomeSheet = 'PRATICHE_DA_BONIFICARE';
	
	nomeFile	= $scope.configMainMenu[$scope.stateParamsUI.section].name;
	
	angular.forEach($scope.configGridUI, function(value,key) {
		console.log(key);
		console.log(value);
		if ((key !=='FUNCTION') && value.visible === true){
			confHeader[key] = {name : value.name, id : value.name, label : value.name, visible : true, enabled : false, type:'string' };
		}
	
	});
	
	dataValue = $scope.data;
	
	// call services macrotasks


		var data = [];
		// faccio logica per definire il set dati da printare
		var dateNow = new Date();
		dateNow = $filter('date')(dateNow,'yyyyMMddHHmmss');
	
		var obj = {
				header	:	confHeader,
				data		:	dataValue
		};
		// ciclo su di esso
		obj = filterNumbers(filterDate(obj));
		console.log(obj);
		_.each(obj.data, function(row) {
			// costruzione nuovo oggetto per la stampa
			var printObj = {};
			_.each(row,function(val,key){
				// restituisce la row modificata con le sole chiavi con visibilita a true
				//console.log(confHeader[key]);
				
				if((confHeader[key]) && (confHeader[key].visible)){ printObj[confHeader[key].label] = val; }
			});
			// add all'array 
			data.push(printObj);
		});

		var ws =  XLSX.utils.json_to_sheet (data);
		// costruttore obj excel
		var wb =  XLSX.utils.book_new ();
		// appendo gli elementi al file
		XLSX.utils.book_append_sheet (wb, ws, nomeSheet );
		// scrivo il file
		XLSX.writeFile(wb, nomeFile+'_'+dateNow+'.xlsx');

		$scope.mostraLoaderUiModal = false;
		
		// jscs:enable requireCamelCaseOrUpperCaseIdentifiers

};	
	
	
	/* cambia pagina */ 
	$scope.pageChanged = function() {
		var startPos = ($scope.currentPage - 1) * $scope.itemsPerPage;
		return startPos;
	};
	
	/* cfg dei campi edit */
	
	$scope.configEditUI = {
		
	};	
	
	
	$scope.configButtonUI = {

	};
	
	/* configurazione della modale invocata dalla UI */
	$scope.configModalUI =	{

	};
	
	
	/* apro la modale con i parametri specifici */
	
	$scope.openModalForm = function (itemSelected,uiAction){
		
		console.log('itemSelectedUI:');
		console.log(itemSelected);
		console.log('actionSelectedUI: '+uiAction);
		
		
		/* UI MODALE - Inizio */
		
		var $ctrl = this;
		
		$ctrl.animationsEnabled = true;
		
		$ctrl.open = function (size, parentSelector,itemSelected,uiChiamante,uiConfig,tab) {
			var modalInstance = $uibModal.open({
				animation: $ctrl.animationsEnabled,
				backdrop:false,
				keyboard:false,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: './views/common/modalBonifica.html',
				controller: 'modalBonificaCtrl',
				controllerAs: '$ctrl',
				size: size,
				//appendTo: parentElem,
				resolve: {
						uiChiamante: function () {
							return uiChiamante;
						},
						itemSelected: function () {
							return itemSelected;
						},
						uiConfig: function () {
							return uiConfig;
						},
						tab: function () {
							return tab;
						}
				}
			});
		
			modalInstance.result.then(function (retObj) {
				console.log(retObj);
				$scope.getGrid();
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};
		
		
		$ctrl.open('fullscreen',null,itemSelected,$scope.configMainMenu[$scope.stateParamsUI.section],$scope.configModalUI,'');

	};
	
	
	/* apro la modale con i parametri specifici */
	
	$scope.openModalFormGestito = function (itemSelected,uiAction){
		
		console.log('itemSelectedUI:');
		console.log(itemSelected);
		console.log('actionSelectedUI: '+uiAction);
		
		
		/* UI MODALE - Inizio */
		
		var $ctrl = this;
		
		$ctrl.animationsEnabled = true;
		
		$ctrl.open = function (size, parentSelector,itemSelected,uiChiamante,uiConfig,tab) {
			var modalInstance = $uibModal.open({
				animation: $ctrl.animationsEnabled,
				backdrop:false,
				keyboard:false,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: './views/common/modalGestitaUfficio.html',
				controller: 'modalGestitaUfficioCtrl',
				controllerAs: '$ctrl',
				size: size,
				//appendTo: parentElem,
				resolve: {
						uiChiamante: function () {
							return uiChiamante;
						},
						itemSelected: function () {
							return itemSelected;
						},
						uiConfig: function () {
							return uiConfig;
						},
						tab: function () {
							return tab;
						}
				}
			});
		
			modalInstance.result.then(function (retObj) {
				console.log(retObj);
				$scope.getGrid();
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};
		
		
		$ctrl.open('small',null,itemSelected,$scope.configMainMenu[$scope.stateParamsUI.section],$scope.configModalUI,'');

	};	

	// procedura che permette di sottomettere la pratica
	$scope.sottomettiHeaderPratica = function(item,azione,chiamante){
		console.log('sottomettiHeaderPratica');
		
		$scope.mostraLoaderGrid = true;
		
		// oggetto parametri da mandare alla funzione di sottomettiHeader
		
		var paramsCall = {
			ID									:	item.ID						,
			ID_SOURCE						:	item.ID_SOURCE		,
			ID_POLARYS					:	item.ID_POLARYS		,
			SOURCE							:	item.SOURCE				,
			TARGET							:	item.TARGET				,
			TIPOINTERVENTO			:	item.TIPOINTERVENTO,
			EVENTO							:	item.EVENTO				,
			STATO_PARTENZA			:	item.STATOPARTENZA,
			CHIAMANTE						:	chiamante
		};
		
		console.log('sottomettiHeader => paramsCall: ',paramsCall);
		
		
		// gestisco la chiamata
		
		var deferred = $q.defer();
		var urlCalls = [];
		
		
		/* richiamo la funzione di movimentazione */
		var esito =	sottomettiHeader.postData(paramsCall).$promise.then(function(resultCall) {
		
			return	{
								ESITO 			: resultCall.data.ESITO,
								DESC_ESITO 	:	resultCall.data.DESC_ESITO
							};
		
		},function(){
				return {
								ESITO 			: 'KO',
								DESC_ESITO 	:	'Errore interno alla funzione sottomettiHeader'
							};
		});
					
		urlCalls.push(esito);
		
		/* gestisco gli esiti */
				
		$q.all(urlCalls)
		.then(
			function(results) {
				deferred.resolve();
		
				console.log(results);
				
				$scope.mostraLoaderGrid = false;
				
				angular.forEach(results, function(esitoChiamata) {
					
					if (esitoChiamata.ESITO === 'OK'){
						$scope.alert.success(esitoChiamata.DESC_ESITO, { ttl: 10000 });
						$scope.getGrid();	
					}
					else{
						$scope.alert.error(esitoChiamata.DESC_ESITO, { ttl: 10000 });
					}
				});

		},
		function(errors) {
			deferred.reject(errors);
			$scope.mostraLoaderGrid = false;
		},
		function(updates) {
			deferred.update(updates);
			$scope.mostraLoaderGrid = false;
		});
	
	};


	//procedura che gestisce l'apertura del link a CS01
	$scope.openLinkToCs01 = function (item,azione){
		window.open("http://was.portale.sirti.net/Applicazioni/CS01/Intervento/"+item.ID_CS01, "_blank");
	};
	//procedura che gestisce l'apertura del link a vedi attivita polarys
	$scope.openLinkToVediActPolarys = function (idAttivita,azione){
		window.open("../PYR_MTZ_NOC_MASTER/VEDI_ATTIVITA/index.html?stile=custom-theme&id_attivita_passata="+idAttivita, "_blank");
	};
}


angular.module('guiMonitorPraticheCs01').controller('gsBonificaPraticheCtrl', gsBonificaPraticheCtrl);
