'use strict';

angular.module('Utils', [])


.factory('DealWithJson', function() {
	return {
		'getArrayLength': function() {
			return function(s) {
				var a = angular.fromJson(s);
				return (angular.isDefined(a) ? a.length : 0);
			};
		}
	};
})

.factory('DealWithNumbers', function() {
	return {
		'getPercentage': function() {
			return function (maxValue, currentValue) {
				maxValue = parseInt(maxValue);
				currentValue = parseInt(currentValue);
				if (!Number.isNaN(maxValue) && angular.isNumber(maxValue) && !Number.isNaN(currentValue) && angular.isNumber(currentValue)){
					return Number.parseInt(currentValue / maxValue * 100);
				} 					
				return 0;
			};
		},
		'toNumber': function() {
			return function (value, defaultValue) {
				value = parseInt(value);
				if (Number.isNaN(value) && angular.isDefined(defaultValue)){
					return defaultValue;
				}else{
					return value;
				}
					
			};
		},
		'isNumber': function() {
			return function(value) {
				return angular.isNumber(value);
			};
		}
	};
})

.factory('RenderPropertyLabel', function() {
	return function(h, m) {
		if(!angular.isObject(h)) {
			return h;
		}
		if (m.template) {
			var keys = Object.keys(h);
			var r = m.template;
			for (var i=0; i<keys.length; ++i) {
				r = r.replace('@{' + keys[i] + '}', h[keys[i]]);
			}
			return r;
		} else {
			var a = [];
			for (var key in h) {
				// diamo per scontato che le chiavi che iniziano per $ sono injected by angular, quindi le skippiamo
				// FIXME: rivedere quando docuemnteremo bene tutte le possibilità
				if(key.match(/^\$/)){ continue; }
				a.push(h[key]);
			}
			return a.join(' - ');
		}
	};
})

.factory('RenderTooltip', function() {
	return function(tooltip) {
		var r = '';
		if (angular.isArray(tooltip)) {
			r += '<ul class="no-wrap;">';
			for (var i=0;i<tooltip.length; ++i) {
				r += '<li>' + tooltip[i].label + ': <strong>' + tooltip[i].value + '</strong></li>';
			} 
			r += '</ul>';
		}
		if (angular.isString(tooltip)) {
			r += tooltip;
		}
		return r;
	};
})

.factory('LoadTags', function() {
	return function(propertyModels) {
		return function($query, key) {
			var tags = propertyModels[key].values;
			return tags.filter(function(tag) {
				return tag.tag.toLowerCase().indexOf($query.toLowerCase()) !== -1;
			});
		};
	};
})

.factory('LoadTagsAsync', function($http, $rootScope, UiConfig) {
	return function(/*propertyModels, scope*/) {
		return function($query, key, uri, context, contextKey) {
			var url = '';
			switch (context) {
				case 'Config':
					//$rootScope.config.CollaudoAppService
					url += UiConfig.getKey(contextKey);
					break;
				/*
				case 'Api':
					url += myConfig.ApiService;
					break;
				case 'App':
					url += myConfig.AppService;
					break;
				*/
			}
			url += uri;
			
			return $http({
				method: 'GET',
				url: url,
				params: { q: $query },
				withCredentials: true
			})/*.
			success(function(data / *, status* /) {
				return data;
			}).
			error(function(err / *, status* /) {
				scope.alert.warning('Unable to perform autocomplete, please contact system administrator', err);
			})*/;
		};
	};
})

.factory('SliderPercentageOptions', function() {
	return {       
		from: 0,
		to: 100,
		step: 5,
		dimension: ' %',
		//scale: [ 0, 25, 50, 75, 100 ],
		skin: 'round',
		css: {
			background: {'background-color': 'silver', 'border': '1px inset'},
			before: {'background-color': 'lightblue', 'border': '1px inset'},
			'default': {'background-color': 'black', 'border': '1px inset'},
			after: {'background-color': 'lightblue', 'border': '1px inset'}
			//range: {"background-color": "silver"} 
		} 
	};
})

.factory('focus', function($timeout) {
	return function(id) {
		var eleFocused = angular.element('#'+id);
		if (eleFocused.length) {
			$timeout(function() {
				eleFocused.trigger('focus');
			});
		}
	};
})

.factory('$', ['$window', function($window) {
	return $window.$; // assumes underscore has already been loaded on the page
}])

.factory('_', ['$window', function($window) {
	return $window._; // assumes underscore has already been loaded on the page
}])

.factory('moment', ['$window', function($window) {
	var lang = window.navigator.userLanguage || window.navigator.language;
	$window.moment.locale(lang);
	return $window.moment; // assumes moment has already been loaded on the page
}])

.factory('sprintf', ['$window', function($window) {
	return $window.sprintf; // assumes sprintf has already been loaded on the page
}])

.factory('XLSX', ['$window', function($window) {
	return $window.XLSX; // assumes sprintf has already been loaded on the page
}])

.directive('eventFocus', function(focus) {
	return function(scope, elem, attr) {
		elem.on(attr.eventFocus, function() {
			focus(attr.eventFocusId);
		});
		// Removes bound events in the element itself
		// when the scope is destroyed
		scope.$on('$destroy', function() {
			elem.off(attr.eventFocus);
		});
	};
})

.directive('focusMe', function($timeout, $parse) {
	return {
		//scope: true,   // optionally create a child scope
		link: function(scope, element, attrs) {
			var model = $parse(attrs.focusMe);
			scope.$watch(model, function(value) {
				if(value === true) { 
					$timeout(function() {
						element[0].focus();
					});
				}
			});
		}
	};
})

//Valuta l'attributo passato breakpoint sintassi 
.directive('breakpoint', function ($window) {
	return function (scope) {
		var matchmedia = {
				rules: {
					print : 'print',
					screen : 'screen',
					phone : '(max-width: 767px)',
					tablet : '(min-width: 768px) and (max-width: 979px)',
					desktop : '(min-width: 980px)',
					bootstrapSwitch :'(min-width: 767px)',
					portrait : '(orientation: portrait)',
					landscape : '(orientation: landscape)'
				}
			};

		var supportsMatchMedia = $window.matchMedia !== undefined && !!$window.matchMedia('all').addListener;
		if(supportsMatchMedia){
			var mql = $window.matchMedia( matchmedia.rules.bootstrapSwitch );
			var mqlHandler = function (mql) {
				scope.matches = mql.matches;
				if(!scope.$$phase) { scope.$apply(); }
			};
			mql.addListener(mqlHandler);
			mqlHandler(mql);
		}

	};
})

/***
 * Direttiva per far funzionare ENTER com TAB
 */
.directive('moveNextOnEnter', function() {
	return function(scope, element) {
		element.bind('keydown keypress', function(event) {
			if (event.which === 13) {
				event.preventDefault();
				var fields = element.parents('form:eq(0),body').find('input, textarea, select');
				var index = fields.index(this);
				if (index > -1 && (index + 1) < fields.length) {
					fields.eq(index + 1).focus();
				}
			}
		});
	};
})

.filter('getKeys', function(_) {
	return function(obj) {
		return _.keys(obj);
	};
})


.filter('propertyBeautifier', function() {
	return function(input) {
		return input.replace(/_/g, ' ').toLowerCase().replace( /\b\w/g, function (m) {
			return m.toUpperCase();
		});
	};
})

.filter('removeUnderscores', function() {
	return function(input, repl) {
		if (!angular.isDefined(repl)) {
			repl = ' ';
		}
		if (input === null) {
			return input;
		}
		if (!angular.isDefined(input)) {
			return input;
		}
		return input.replace(/_/g, repl);
	};
})

.filter('limitString', function() {
	return function(input, size) {
		return (input.length > size ? input.substr(0, size).trim() + '...' : input);
	};
})

.filter('sortAndFilter', function($filter) {
	// la funzione cerca e ordina 
	return function(input,sortByfield,search) {
		var result = input;
		result = $filter('filter')($filter('orderBy')(result,sortByfield), search);
		return result;
	};
})

.filter('capitalize', function() {
	return function(input) {
		if (input !== null) {
			var stringArr = input.split(' ');
			var result = '';
			var cap = stringArr.length;
			for(var x = 0; x < cap; x++) {
				stringArr[x].toLowerCase();
				if(x === cap - 1) {
					result += stringArr[x].substring(0,1).toUpperCase() + stringArr[x].substring(1);
				} else {
					result += stringArr[x].substring(0,1).toUpperCase() + stringArr[x].substring(1) + ' ';
				}
			}
			return result;
		}
	};
})

// gestisce la disabililitazione dei link ui-sref passando una funzione di callBack oppure la var true / false
// come attributo ui-sref-if="var"
.directive('uiSrefIf', ['$parse', '$rootScope',
	function($parse, $rootScope) {
	return {
		// this ensure eatClickIf be compiled before ngClick
		priority: 100,
		restrict: 'A',
		compile: function($element, attr) {
			var fn = $parse(attr.uiSrefIf);
			return {
				pre: function link(scope, element) {
					var eventName = 'click';
					element.on(eventName, function(event) {
						var callback = function() {
							if (fn(scope, {$event: event})) {
								// prevents ng-click to be executed
								event.stopImmediatePropagation();
								// prevents href 
								event.preventDefault();
								return false;
							}
						};
						if ($rootScope.$$phase) {
							scope.$evalAsync(callback);
						} else {
							scope.$apply(callback);
						}
					});
				},
				post: function() {}
			};
		}
	};
}
])

/**
 * Restituisce il nome del file dall'intestazione 'content-disposition'
 * FIXME: trovare un metodo più robusto di split()[1].trim().split()[1]
 */
.service('getFilenameFromHeader', function() {  
	return function(headers) {
		var result = '';
		var header = headers('content-disposition');
		if (header) {
			result = header.split(';')[1].trim().split('=')[1];
			result = result.replace(/"/g, '');
		}
		return result;
	};
})

/**
 * Gestisce la conversione da blob a json in caso di responseType='blob' 
 * Nota: la promise serve per consentire al chiamante di eseguire (eventuali) ulteriori .catch()
 */
.service('BlobResourceError', function($q, $timeout) {
	return function(error) {
		var deferred = $q.defer();
		var reader = new FileReader();
		reader.onload = function() {
			error.data = angular.fromJson(reader.result);
			deferred.reject(error);
		};
		reader.onerror = function(err) {
			deferred.reject(err);
		};
		try {
			reader.readAsText(error.data.data);
		} catch(err) {
			$timeout(function() {
				deferred.reject(err);
			});
		}
		return deferred.promise; 
	};
})

/**
 * Esegue il download dell'allegato descritto da options.
 * (tutorial: http://blog.neilni.com/2016/04/23/download-file-in-angular-js/)
 */
.factory('DownloadService', function (BlobResourceError, $timeout) {
	
	return function(downloadService, options, next, err) {
		downloadService(options)
			.$promise
			.then(function(result) {
				/*
				 * If response is successful, convert the data from ArrayBuffer into Blob, 
				 * create a temporary url from the blob, and then programatically create a 
				 * link to download.
				 * 
				 * NOTA: result viene valorizzato dalla chiave 'transformResponse' della $resource
				 */
				if (window.navigator.msSaveOrOpenBlob) {
					// IE11
					window.navigator.msSaveOrOpenBlob(result.data, result.filename);
					next();
				} else {
					// Chrome e FF
					var url = URL.createObjectURL(result.data);
					var a = document.createElement('a');
					a.href = url;
					a.download = result.filename;
					a.target = '_blank';
					document.body.appendChild(a);	// Indispensabile x il funzionamento in Firefox
					// Il doppio $timeout serve per la compatibilità fra Chrome e Firefox 
					$timeout(function() {
						a.click();
						$timeout(function() {
							document.body.removeChild(a);		// Rimuove dal DOM l'anchor
							window.URL.revokeObjectURL(url);    
							next();
						}, 0);
					}, 0);
				}
			})
			.catch(
				/*
				 * Siccome la rotta restituisce dati blob, per gestire l'eventuale errore
				 * è necessario convertire il payload in JSON
				 */
				BlobResourceError
			)
			.catch(function(error) {
				err(error);
			});
	};
	
})

// format number to string
.factory('filterNumbers', function(_) {
	return function(data) {
		_.each(data.header , function(h , k){
			if(h.type === 'number'){
				_.each(data.data , function(d){
					if(!_.isNull(d[k]) && _.isString(d[k])){
						d[k] = Number(d[k]) || '';
					}
				});
			}
		});
		return data;
	};
})
// format date to string
.factory('filterDate', function(_,moment) {
	return function(data) {
		_.each(data.header , function(h , k){
			if(h.type === 'date'){
				_.each(data.data , function(d){
					if(!_.isNull(d[k]) && _.isString(d[k])){
						var lang = window.navigator.userLanguage || window.navigator.language;
						// validation date 
						if(moment(d[k], 'DD/MM/YYYY HH:mm:ss',lang,true).isValid()){
							d[k] = moment(d[k], 'DD/MM/YYYY HH:mm:ss',lang,true)._d;
						}else if(moment(d[k], 'DD/MM/YYYY HH:mm',lang,true).isValid()){
							d[k] = moment(d[k], 'DD/MM/YYYY HH:mm',lang,true)._d; 
						}else if(moment(d[k], 'DD/MM/YYYY',lang,true).isValid()){
							d[k] = moment(d[k], 'DD/MM/YYYY',lang,true)._d;
						}else{return;}
					}
				});
			}
		});
		return data;
	};
})

//Code goes here
.factory('Excel',function($window,$){
	var uri='data:application/vnd.ms-excel;base64,';
	var template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
	/*jshint nonstandard:true */
	var base64 = function(s){ return $window.btoa(unescape(encodeURIComponent(s))); };
	/*jshint nonstandard:false */
	var format = function(s,c){ return s.replace(/{(\w+)}/g,function(m,p){return c[p];}); };
	return {
		tableToExcel: function(tableId , worksheetName , fileName){
			var table=$(tableId);
			var ctx={worksheet:worksheetName,table:table.html()};
			var link = document.createElement('a');
			link.download = fileName;
			link.href = uri + base64(format(template, ctx));
			return link.href;
		}
	};
})

// validator mails list filed type text
.directive('multipleEmails', function () {
	return {
		require: 'ngModel',
		link: function(scope, element, attrs, ctrl) {
			ctrl.$parsers.unshift(function(viewValue) {

				var emails = viewValue.split(',');
				// define single email validator here
				var re = /\S+@\S+\.\S+/; 

				// angular.foreach(emails, function() {
				var validityArr = emails.map(function(str){
					return re.test(str.trim());
				}); // sample return is [true, true, true, false, false, false]
				console.log(emails, validityArr); 
				var atLeastOneInvalid = false;
				angular.forEach(validityArr, function(value) {
					if(value === false){
						atLeastOneInvalid = true;
					}
				}); 
				if(!atLeastOneInvalid) { 
					// ^ all I need is to call the angular email checker here, I
					// think.
					ctrl.$setValidity('multipleEmails', true);
					return viewValue;
				} else {
					ctrl.$setValidity('multipleEmails', false);
					return undefined;
				}
				// })
			});
		}
	};
})
;
